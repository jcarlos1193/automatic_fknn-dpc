function [ labels_cluster, centers, outliers, best_purity, best_vmeasure, best_K, best_cputime ] = FKNN_DPC_improved_v3( X_initial, NCLUST, labels_class, name_dataset )

    percent_pca = 95;

    while true

        best_information_gain = -Inf;
        best_purity = 0;
        best_vmeasure = 0;
        best_K = NaN;
        best_cputime = NaN;

        list_vmeasure = [];
        list_purity = [];
        list_K = [];
        list_cputime = [];
    
        [~,~,~,~,explained] = pca(X_initial);
        cs = cumsum(explained);
        num_components = find( cs > percent_pca );
        [~,X] = pca(X_initial, 'NumComponents', num_components(1));

        dist_vector = pdist(X);
        dist = squareform(dist_vector);

        l_k_values = linspace(1, 50, 50);

        for K = l_k_values

            K = round(K);

            timerVal = tic;
            [labels_cluster, centers, outliers] = FKNN_DPC(X, NCLUST, K, dist);
            total_time = toc(timerVal);

            purity_value = purity_v2(labels_class, labels_cluster);
            [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, labels_cluster);

            fprintf('|Purity: %f', purity_value*100);
            fprintf('|\t|Vmeasure: %f', vmeasure*100);
            fprintf('|\t|K: %d', K);
            fprintf('|\t|CPU time: %f (s)|\n', total_time);

            list_vmeasure = [list_vmeasure, vmeasure];
            list_purity = [list_purity, purity_value];
            list_K = [list_K, K];
            list_cputime = [list_cputime, total_time];

            % Compute Information Gain

            list_clusters = unique(labels_cluster);

            entropy_clusters = 0;

            for idx = list_clusters
                index_data = find(labels_cluster == idx);
                partition = X(index_data, :);
                aux_value = -(size(partition,1)/size(X,1)) * log(det( cov( partition ) + 0.01 * eye(size(X,2)) ));
                entropy_clusters = entropy_clusters + aux_value;
            end

            if entropy_clusters == Inf
                percent_pca = percent_pca - 5;
                break; 
            end
            
            information_gain = log(det( cov( X ) + 0.01 * eye(size(X,2)) )) + entropy_clusters;
            fprintf('PCA: %d%%, Information gain: %.2f\n\n', percent_pca, information_gain);

            if best_information_gain < information_gain

                best_purity = purity_value;
                best_information_gain = information_gain;
                best_vmeasure = vmeasure;
                best_K = K;
                best_cputime = total_time;

            end

        end
        
        if K == l_k_values(length(l_k_values)) && entropy_clusters ~= Inf
            break;
        end
        
    end
    
    str_filename = sprintf('Results/FKNN-DPC-Impr-v3_%s.mat',name_dataset);
    save(str_filename, 'list_vmeasure', 'list_purity', 'list_K', 'list_cputime');

end

