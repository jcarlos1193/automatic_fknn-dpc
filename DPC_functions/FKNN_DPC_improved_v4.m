function [ best_global_cl, best_global_icl, best_purity, best_vmeasure, best_K, best_cputime ] = FKNN_DPC_improved_v4( X_initial, NCLUST, labels_class, name_dataset )

    percent_pca = 95;

    while true

        best_global_information_gain = -Inf;
        best_global_icl = [];
        best_global_cl = [];
        
        best_purity = 0;
        best_vmeasure = 0;
        best_K = NaN;
        best_cputime = NaN;

        list_vmeasure = [];
        list_purity = [];
        list_K = [];
        list_cputime = [];
        list_information_gain = [];
    
        [~,~,~,~,explained] = pca(X_initial);
        cs = cumsum(explained);
        num_components = find( cs > percent_pca );
        [~,X] = pca(X_initial, 'NumComponents', num_components(1));

        dist_vector = pdist(X);
        dist = squareform(dist_vector);

        l_k_values = linspace(1, 50, 50);

        for K = l_k_values

            K = round(K);

            timerVal = tic;
            
            % compute density and distances
            [rho, KNN] = DensityFKNNDPC(X, K);
            [delta, ~, ~] = DELTA(dist, rho);
            
            % Choose automatically the best gamma centers

            gamma = rho .* delta;
            [~, icl_aux] = sort(gamma, 'descend');

            rhomin = 0.0;
            deltamin = 0.0;
            icl_initial = [];

            for i = 1 : length(icl_aux)

                if ( (rho(icl_aux(i)) > rhomin) && (delta(icl_aux(i)) > deltamin) )
                    icl_initial = [icl_initial, icl_aux(i)];
                end

                if length(icl_initial) == NCLUST
                    break;
                end

            end
            
            icl = icl_initial;
            num_iters = 150;
            num_centroids = round(0.7 * (NCLUST - 1));
            
            best_cl = [];
            best_icl = [];
            best_information_gain = -Inf;
            
            for ixaux = 1 : num_iters
                
                if ixaux ~= 1
                    
                    lprobabilities = gamma(icl(2:length(icl))) ./ sum(gamma(icl(2:length(icl))));
                    
                    if length(lprobabilities) ~= 1
                        lprobabilities2 = 1 - lprobabilities;
                        lprobabilities = lprobabilities2 ./ sum(lprobabilities2);
                    end
                    
                    l_cl_chosen = [];

                    while true

                        ix_centroid = sum( rand >= cumsum(lprobabilities) ) + 1;
                        ix_centroid = icl(ix_centroid + 1);

                        if ismember(ix_centroid, l_cl_chosen)
                            continue;
                        end

                        l_cl_chosen = [l_cl_chosen, ix_centroid];

                        if length(l_cl_chosen) == num_centroids
                            break;
                        end

                    end
                    
                    [icl_aux_neighs, ~] = knnsearch(X, X(l_cl_chosen, :), 'K', round(0.03 * size(X, 1)));
                    icl_aux_neighs(:,1) = [];
                    
                    idx_icl = 1;

                    while true

                        icl_aux = l_cl_chosen(idx_icl);

                        lprobabilities = gamma(icl_aux_neighs(idx_icl,:)) ./ sum(gamma(icl_aux_neighs(idx_icl,:)));
                        ix_centroid = sum( rand >= cumsum(lprobabilities) ) + 1;
                        
                        if ismember(icl_aux_neighs(idx_icl, ix_centroid), icl)
                            continue;
                        end
                        
                        i_aux = find(icl == icl_aux);
                        icl(i_aux) = icl_aux_neighs(idx_icl, ix_centroid);
                        
                        if idx_icl == length(l_cl_chosen)
                            break;
                        else  
                            idx_icl = idx_icl + 1;
                        end
                        
                    end

                end

                cl = -1 * ones(1, size(X, 1));

                for idx = 1 : NCLUST
                    cl(icl(idx)) = idx;
                end

                % detecting outliers
                out = OUTLIERS(KNN, dist);

                % Use strategy 1
                cl = Strategy1(icl, KNN, dist, K, out, cl);

                % Use strategy 2
                cl = Strategy2(icl, KNN, dist, cl);

                labels_cluster = cl;

                % Compute Information Gain

                list_clusters = unique(labels_cluster);

                entropy_clusters = 0;

                for idx = list_clusters
                    index_data = find(labels_cluster == idx);
                    partition = X(index_data, :);
                    aux_value = -(size(partition,1)/size(X,1)) * log(det( cov( partition ) + 0.01 * eye(size(X,2)) ));
                    entropy_clusters = entropy_clusters + aux_value;
                end

                if entropy_clusters == Inf
                    percent_pca = percent_pca - 5;
                    break; 
                end

                information_gain = log(det( cov( X ) + 0.01 * eye(size(X,2)) )) + entropy_clusters;
                fprintf('PCA: %d%%, Information gain: %.2f\n\n', percent_pca, information_gain);
                
                if best_information_gain < information_gain
                    
                    best_information_gain = information_gain;
                    best_cl = labels_cluster;
                    best_icl = icl;

                else
                    icl = best_icl;
                end
                
            end
            
            if entropy_clusters == Inf
                break;
            end
            
            total_time = toc(timerVal);
            
            purity_value = purity_v2(labels_class, best_cl);
            [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, best_cl);

            fprintf('|Purity: %f', purity_value*100);
            fprintf('|\t|Vmeasure: %f', vmeasure*100);
            fprintf('|\t|K: %d', K);
            fprintf('|\t|CPU time: %f (s)|\n', total_time);

            list_vmeasure = [list_vmeasure, vmeasure];
            list_purity = [list_purity, purity_value];
            list_K = [list_K, K];
            list_cputime = [list_cputime, total_time];
            list_information_gain = [list_information_gain, best_information_gain];
            
            if best_global_information_gain < best_information_gain

                best_global_information_gain = best_information_gain;
                best_global_icl = best_icl;
                best_global_cl = best_cl;
                
                best_purity = purity_value;
                best_vmeasure = vmeasure;
                best_K = K;
                best_cputime = total_time;

            end

        end
        
        if K == l_k_values(length(l_k_values)) && entropy_clusters ~= Inf
            break;
        end
        
    end
    
    str_filename = sprintf('Results/FKNN-DPC-Impr-v4_%s.mat',name_dataset);
    save(str_filename, 'list_vmeasure', 'list_purity', 'list_K', 'list_cputime', 'list_information_gain');

end

