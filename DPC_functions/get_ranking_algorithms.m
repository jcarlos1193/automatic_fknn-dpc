function [T_paired_rank, mr, p_value] = get_ranking_algorithms(array_results, alg_names)

% Friedman test

[p_value, tbl, stats] = friedman(array_results, 1, 'off');

% Paired rank comparison

mr = stats.meanranks;
paired_rank = zeros(length(mr));

for idx = 1 : length(mr)
    
    for idx2 = idx + 1 : length(mr)
        
        paired_rank(idx, idx2) = mr(idx) - mr(idx2);
        
    end
    
end

% Build a table with results

T_paired_rank = array2table(paired_rank, 'VariableNames', alg_names, 'RowNames', alg_names);

end
