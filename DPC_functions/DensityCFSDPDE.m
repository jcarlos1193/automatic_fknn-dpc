function rho=DensityCFSDPDE(dist,dc,label)
% observation dist is the square distance
dc=dc^2;
ND=size(dist,2); % number of columns
rho=zeros([1,ND]);
if label==1     % Gaussian kernel
    for j=1:ND
        rho(j)=sum(exp(-dist(:,j)/dc));
    end
elseif label==2 % "Cut off" kernel
    for j=1:ND
        rho(j)=length(find(dist(:,j)<dc));
    end
end
end
