function rho=Density(dist,ND,dc,label)
rho=zeros([1,ND]);

%knnsearch
% Gaussian kernel
%
if label==1
    for i=1:ND-1
        for j=i+1:ND
            rho(i)=rho(i)+exp(-(dist(i,j)/dc)*(dist(i,j)/dc));
            rho(j)=rho(j)+exp(-(dist(i,j)/dc)*(dist(i,j)/dc));
        end
    end
elseif label==2
    %
    % "Cut off" kernel
    %
    for i=1:ND-1
        for j=i+1:ND
            if (dist(i,j)<dc)
                rho(i)=rho(i)+1.;
                rho(j)=rho(j)+1.;
            end
        end
    end
end
end
