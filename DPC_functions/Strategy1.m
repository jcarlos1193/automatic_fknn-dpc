function clu=Strategy1(CI,KNN,dist,K,out,clu)
[ND,m]=size(dist);
%clu=ones([1,ND]);
% CI centroides
% KNN entornos
% dist matriz de distancia
% K n�mero de vecinos
% out outliers
CLUST=length(CI);
for i=1:CLUST
    c=CI(i);
    clu(KNN(c,:))=i;% asigna todos los del entorno de c a c
    Q=KNN(c,:);

    while length(Q)>0
        p=Q(1);
        Q=Q(2:end);
        for q=KNN(p,:)
            aux=length(find(out==q));
            dist_aux=sum(dist(q,KNN(q,:)))/K;
            if (clu(q)==-1 & aux==0 & dist(p,q)<=dist_aux)
                clu(q)=clu(p);
                Q=[Q q];
            end
        end
        
    end
end

end