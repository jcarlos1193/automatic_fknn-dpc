function out=OUTLIERS(KNN,dist)
% eq (6)
[N,k]=size(KNN);
for i=1:N
deltaK(i)=max(dist(i,KNN(i,:)));
end
tau=mean(deltaK);
out=find(deltaK>tau);


end