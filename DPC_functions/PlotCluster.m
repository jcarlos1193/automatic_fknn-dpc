function PlotCluster(X,cl,NCLUST)
cmap=colormap;
%figure
[N,D]=size(X);

for i=1:N
   ic=int8((cl(i)*64.)/(NCLUST*1.));
   hold on
   plot(X(i,1),X(i,2),'o','MarkerSize',8,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
end


end