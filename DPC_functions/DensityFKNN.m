function [rho,n2]=DensityFKNN-DPC(X,K)
[ND,D]=size(X);
rho=zeros([1,ND]);
[n2,d2] = knnsearch(X,X,'k',K);
for i=1:ND
    rho(i)= sum(exp(-d2(i,:)));    
end
%knnsearch

end