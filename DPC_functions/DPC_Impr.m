function [best_global_cl, best_global_icl, best_dc] = DPC_Impr(X, NCLUST, labels_class, dist, dist_vector)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matriz distancia
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = length(dist_vector);
ND = size(X,1);
sda = sort(dist_vector);
maxd = max(max(dist));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcular rango dc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ldc = [];

for percent_dc = 0.5 : 0.5 : 25

    position = round(N * percent_dc / 100);
    ldc = [ldc, sda(position)];

end

dc_low = min(ldc);
dc_up = max(ldc);

if dc_low == 0
    dc_low = 0.01;
end

ldc = linspace(dc_low, dc_up, 50);

best_dc = nan;
best_global_cl = [];
best_global_icl = [];
best_global_vmeasure = 0;

for dc = ldc 

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calcular rho, densidad de cada punto
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    for i = 1 : ND
        rho(i) = 0.;
    end

    % Gaussian kernel

    for i = 1 : ND - 1
      for j = i + 1 : ND
         rho(i) = rho(i) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
         rho(j) = rho(j) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
      end
    end

    [~, ordrho] = sort(rho, 'descend');


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calcular delta
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    delta(ordrho(1)) = -1.;
    nneigh(ordrho(1)) = 0;

    for ii = 2 : ND

        delta(ordrho(ii)) = maxd;

        for jj = 1 : ii - 1

            if(dist(ordrho(ii),ordrho(jj)) < delta(ordrho(ii)))
                delta(ordrho(ii)) = dist(ordrho(ii),ordrho(jj));
                nneigh(ordrho(ii)) = ordrho(jj);
            end

        end

    end

    delta(ordrho(1)) = max(delta(:));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Seleccion de centroides para identificar los clusters automatizada
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    gamma = rho .* delta;
    [~, icl_aux] = sort(gamma, 'descend');

    rhomin = 0.0;
    deltamin = 0.0;
    icl = [];

    for i = 1 : length(icl_aux)

        if ( (rho(icl_aux(i)) > rhomin) && (delta(icl_aux(i)) > deltamin) )
            icl = [icl,icl_aux(i)];
        end

        if length(icl) == NCLUST
            break;
        end

    end

    
    %% -------------------------------------------------------------
    
    icl_old = icl; % pensar en si se actualiza con el mejor icl encontrado
    num_iters = 150;
    num_centroids = round(0.7 * (NCLUST - 1)); % no se considera el mejor centroide
    best_cl = [];
    best_icl = [];
    best_vmeasure = 0;
    
    % 1. Ajustar epsilon para escoger las hiperesferas
    
    percent_epsilon = 0.85;
    
    d_rho = abs(max(rho) - min(rho));
    d_delta = abs(max(delta) - min(delta));
    min_value = min([d_rho, d_delta]);
    
    epsilon = min_value * percent_epsilon;
    
    % ---
    
    for ixaux = 1 : num_iters

        if ixaux ~= 1
        
            lprobabilities = gamma(icl_old(2:length(icl_old))) ./ sum(gamma(icl_old(2:length(icl_old))));
            l_cl_chosen = [];
            
            while true % elegir los num_centroids del vector icl_old, que es el original
            
                ix_centroid = sum( rand >= cumsum(lprobabilities) ) + 1;
                ix_centroid = icl_old(ix_centroid + 1); % para descartar al mejor centroide que es el primeroz

                if ismember(ix_centroid, l_cl_chosen)
                    continue;
                end

                l_cl_chosen = [l_cl_chosen, ix_centroid];
                
                if length(l_cl_chosen) == num_centroids
                    break;
                end
                
            end
            
            % 2. para cada punto de l_cl_chosen, coger la lista de vecinos
            % dentro de la distancia d(rho,delta) < epsilon
            
            icl = icl_old;
            
            for idx_icl = 1 : length(l_cl_chosen)
                
                icl_aux_neighs = [];
                icl_aux = l_cl_chosen(idx_icl);
                
                for idx_cl = 1 : ND
                    
                    if ismember(idx_cl, icl_old) || ismember(idx_cl, icl)
                        continue;
                    end
                    
                    pair_dist = pdist2([rho(idx_cl), delta(idx_cl)], [rho(icl_aux), delta(icl_aux)]);
                    
                    if (pair_dist <= epsilon && pair_dist > 0)
                        icl_aux_neighs = [icl_aux_neighs, idx_cl];
                    end
                    
                end
                
                if isempty(icl_aux_neighs)
                    continue; 
                end
                
                % 3. el punto aleatorio (con probabilidad proporcional a gamma)
                % seleccionado para cada punto l_cl_chosen, intercambiarlo para
                % que pase a la lista icl que sera evaluada
                
                lprobabilities = gamma(icl_aux_neighs) ./ sum(gamma(icl_aux_neighs));
                
                ix_centroid = sum( rand >= cumsum(lprobabilities) ) + 1;
                i_aux = find(icl_old == icl_aux);
                
%                 icl = [icl, icl_aux_neighs(ix_centroid)];
                icl(i_aux) = icl_aux_neighs(ix_centroid);
                
            end
           
            % 4. pensar si la lista icl si es la mejor convertirla en
            % icl_old al obtener una mejora o por iteracion
            
            icl_old = icl;
            
            % 5. pensar en si el criterio de valor de ajuste puede ser la
            % informacion ganada para que sea no supervisado
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        end

        % ---------------------------------------------------------------

        cl = -1 * ones(1, ND);

        for idx = 1 : NCLUST
            cl(icl(idx)) = idx;
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Asignacion de los datos a sus clusters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        for i = 1 : ND
            if (cl(ordrho(i)) == -1)
                cl(ordrho(i)) = cl(nneigh(ordrho(i)));
            end
        end

        [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, cl);

        if best_vmeasure < vmeasure
            best_vmeasure = vmeasure;
            best_cl = cl;
            best_icl = icl;
        end
        
        disp(ixaux);
        
    end
    
    if best_global_vmeasure < best_vmeasure
        best_global_vmeasure = best_vmeasure;
        best_global_cl = best_cl;
        best_dc = dc;
        best_global_icl = best_icl;
    end

end


end

