function [VS,rho_midle]=varsigma(D,dc,dist_V,rho,label)
% D distancia dato centroide
% dist_V distnacia centroide centroide
nV=size(D,2);
VS=zeros([nV,nV]);
for i=1:nV-1
    for j=i+1:nV
        if dist_V(i,j)<dc
            VS(i,j)=1;
            VS(j,i)=1;
        elseif dist_V(i,j)<2*dc 
            rho_midle(i,j)=DensityMidle(D,i,j,dist_V(i,j),dc,label);
            if rho_midle(i,j)>=min(rho(i),rho(j))
            VS(i,j)=1;
            VS(j,i)=1;
            end     
        end
            
    end
end
end