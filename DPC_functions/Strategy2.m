function clu=Strategy2(CI,KNN,dist,clu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% initialization: 
%%%        computing the recognition matrix A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ND=size(dist,1);
K=size(KNN,2);
NCLUST=length(CI);
ind=find(clu==-1);  % indexex no assigned
m=length(ind);
for i=1:m
w(i,:)=ones([1,K])./(ones([1,K])+dist(ind(i),KNN(ind(i),:)));
end
for i=1:m
den_gamma(i)=sum(w(i,:));
end

gamma=w./repmat(den_gamma',1,K);
A=gamma.*w; % recognition matrix  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% membership
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:m
for c=1:NCLUST
ind_i=find(clu(KNN(ind(i),:))==c);
pc(i,c)=sum(A(i,ind_i));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computation of VA and VP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:m
    VA(i)=max(pc(i,:));
    aux=find(pc(i,:)==VA(i));
    VP(i)=aux(1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% assignation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:m
    
    max_p=max(VA);
    i_p_aux=find(VA==max_p);
    i_p=i_p_aux(1);
    c=VP(i_p);
    %step 4
    VA(i_p)=-1;
    VP(i_p)=-1;
    p=ind(i_p); % index of the point in the original dataset
    clu(p)=c;   % assignation of cluster for the point p
    % actualizacion de membership
    %fprintf('i= %6i  dato= %6i centroide=%6i\n',i,i_p,c);
    % actualizacion
%     for ii=1:m
%     for cc=1:NCLUST
%     ind_i=find(clu(KNN(ind(ii),:))==cc);
%     pc(ii,cc)=sum(A(ii,ind_i));
%     end
%     end
    q_index=find(VA>=0); % vector de posiciones a modificar
    for j=q_index
        j_q=find(KNN(ind(j),:)==p);
        if (length(j_q)>0)
            pc(j,c)=pc(j,c)+A(j,j_q); 
            VA(j)=max(pc(j,:));
            aux=find(pc(j,:)==VA(j));
            VP(j)=aux(1);
        end              
    end
    
end
end







