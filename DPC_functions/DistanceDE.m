function [delta,ordrho,nneigh]=DistanceDE(dist,rho,VS)
ND=length(rho);
%maxd=max(max(dist));
[rho_sorted,ordrho]=sort(rho,'descend');
delta(ordrho(1))=-1.;
nneigh(ordrho(1))=0;

for ii=2:ND
   delta(ordrho(ii))=max(dist(ii,:));
   for jj=1:ii-1
     if((dist(ordrho(ii),ordrho(jj))<delta(ordrho(ii))) & VS(ordrho(ii),ordrho(jj))==1 )
        delta(ordrho(ii))=dist(ordrho(ii),ordrho(jj));
        nneigh(ordrho(ii))=ordrho(jj);
     end
   end
end
delta(ordrho(1))=max(delta(:));
end