function [cl,icl,out]=FKNN_DPC(X,NCLUST,K,dist)
% step 2: compute density and distances
[rho,KNN]=DensityFKNNDPC(X,K);
[delta,ordrho,nneigh]=DELTA(dist,rho);
% step 3: choice the centers
[cl,icl]=ChoiceCenters(rho,delta,NCLUST);
% step 4: detecting outliers
 out=OUTLIERS(KNN,dist);
% step 5:  Aplicar estrategia 1, CI=icl
cl=Strategy1(icl,KNN,dist,K,out,cl);
%step 6:  Aplicar estrategia 2, CI=icl
cl=Strategy2(icl,KNN,dist,cl);
end