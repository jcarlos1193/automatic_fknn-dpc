function clu_X=CFSFDP_DE(X,dc,MetDen,Km,NCLUST)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% algoritmos CFSFDP_DE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% k-means for examplers
[ND,D]=size(X);
[label,V,sumD,D]=kmeans(X,Km,'replicates',10);
rho=DensityCFSDPDE(D,dc,MetDen);
dist_vector_V=pdist(V);
dist_V=squareform(dist_vector_V);
[VS,rho_m]=varsigma(D,dc,dist_V,rho,MetDen);
[delta,ordrho,nneigh]=DistanceDE(dist_V,rho,VS);

[cl,icl]=ChoiceCenters(rho,delta,NCLUST); 
% 
%assignation
for i=1:Km
  if (cl(ordrho(i))==-1)
    cl(ordrho(i))=cl(nneigh(ordrho(i)));
  end
end
%halo
for i=1:Km
  halo(i)=cl(i);
end
if (NCLUST>1)
  for i=1:NCLUST
    bord_rho(i)=0.;
  end
  for i=1:Km-1
    for j=i+1:Km
      if ((cl(i)~=cl(j))&& (dist(i,j)<=dc))
        rho_aver=(rho(i)+rho(j))/2.;
        if (rho_aver>bord_rho(cl(i))) 
          bord_rho(cl(i))=rho_aver;
        end
        if (rho_aver>bord_rho(cl(j))) 
          bord_rho(cl(j))=rho_aver;
        end
      end
    end
  end
  for i=1:Km
    if (rho(i)<bord_rho(cl(i)))
      halo(i)=0;
    end
  end
end
for i=1:NCLUST
  nc=0;
  nh=0;
  for j=1:Km
    if (cl(j)==i) 
      nc=nc+1;
    end
    if (halo(j)==i) 
      nh=nh+1;
    end
  end
  fprintf('CLUSTER: %i CENTER: %i ELEMENTS: %i CORE: %i HALO: %i \n', i,icl(i),nc,nh,nc-nh);
end

d_aux=(2*dc)^2;% distancia al cuadrado
for j=1:ND
    if D(j,label(j))<d_aux
    clu_X(j)=cl(label(j));
    end
end
end
