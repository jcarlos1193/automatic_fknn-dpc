function dc=DC(dist,percent,N);
position=round(N*percent/100);
sda=sort(dist);
dc=sda(position);

fprintf('Computing Rho with gaussian kernel of radius: %12.6f\n', dc);
end