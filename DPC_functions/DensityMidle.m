function rho=DensityMidle(D,i,j,dvij,dc,label)
distancia=sqrt(0.5*D(:,i)+0.5*D(:,j) -0.25*(dvij^2));
% D contine la distancia euclidea al cuadrado
if label==1     % Gaussian kernel
    rho=sum(exp(-(distancia/dc).*(distancia/dc)));
elseif label==2 % "Cut off" kernel  
    rho=length(find(distancia<dc));
end
end