function [ labels_cluster, centers, outliers, best_purity, best_vmeasure, best_K, best_cputime ] = FKNN_DPC_improved( X, NCLUST, labels_class, name_dataset )

    best_vmeasure = 0;
    best_purity = 0;
    best_K = NaN;
    best_cputime = NaN;
    
    list_vmeasure = [];
    list_purity = [];
    list_K = [];
    list_cputime = [];
    
    dist_vector = pdist(X);
    dist = squareform(dist_vector);

    l_k_values = linspace(1, 50, 50);
    
    for K = l_k_values
        
        K = round(K);
        
        timerVal = tic;
        [labels_cluster, centers, outliers] = FKNN_DPC(X, NCLUST, K, dist);
        total_time = toc(timerVal);
        
        purity_value = purity_v2(labels_class, labels_cluster);
        [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, labels_cluster);

        fprintf('|Purity: %f', purity_value*100);
        fprintf('|\t|Vmeasure: %f', vmeasure*100);
        fprintf('|\t|K: %d', K);
        fprintf('|\t|CPU time: %f (s)|\n\n', total_time);
        
        list_vmeasure = [list_vmeasure, vmeasure];
        list_purity = [list_purity, purity_value];
        list_K = [list_K, K];
        list_cputime = [list_cputime, total_time];
        
        if best_vmeasure < vmeasure
        
            best_purity = purity_value;
            best_vmeasure = vmeasure;
            best_K = K;
            best_cputime = total_time;
        
        end
        
    end
    
    str_filename = sprintf('Results/FKNN-DPC-Impr_%s.mat',name_dataset);
    save(str_filename, 'list_vmeasure', 'list_purity', 'list_K', 'list_cputime');

end

