function [cl, icl, halo] = DPC_Original(X, NCLUST, percent, dist, dist_vector)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matriz distancia
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ND = size(X,1);
maxd = max(max(dist));
N = length(dist_vector);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcular rho, densidad de cada punto
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

position = round( N * percent / 100);
sda = sort(dist_vector);
dc = sda(position);

rho = zeros(1, ND);

% Gaussian kernel

for i = 1 : ND - 1
    for j = i + 1 : ND
        rho(i) = rho(i) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
        rho(j) = rho(j) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
    end
end

[~, ordrho] = sort(rho, 'descend');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcular delta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

delta(ordrho(1)) = -1.;
nneigh(ordrho(1)) = 0;

for ii = 2 : ND

    delta(ordrho(ii)) = maxd;
    
    for jj = 1 : ii - 1
        
        if(dist(ordrho(ii),ordrho(jj)) < delta(ordrho(ii)))
            delta(ordrho(ii)) = dist(ordrho(ii),ordrho(jj));
            nneigh(ordrho(ii)) = ordrho(jj);
        end
     
    end
    
end

delta(ordrho(1)) = max(delta(:));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Seleccion de centroides para identificar los clusters automatizada
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gamma = rho .* delta;
[~, icl_aux] = sort(gamma, 'descend');

rhomin = 0.0;
deltamin = 0.0;
icl = [];

for i = 1 : length(icl_aux)
    
    if ( (rho(icl_aux(i)) > rhomin) && (delta(icl_aux(i)) > deltamin) )
        icl = [icl,icl_aux(i)];
    end
    
    if length(icl) == NCLUST
        break;
    end
    
end

cl = -1 * ones(1, ND);

for idx = 1 : NCLUST
    cl(icl(idx)) = idx;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Asignacion de los datos a sus clusters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1 : ND
    if (cl(ordrho(i)) == -1)
        cl(ordrho(i)) = cl(nneigh(ordrho(i)));
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Halo, o zona de ruido
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

halo = cl;

if (NCLUST > 1)
    
    bord_rho = zeros(1, NCLUST);
  
    for i = 1 : ND - 1
        
        for j = i + 1 : ND
            
            if ((cl(i) ~= cl(j)) && (dist(i,j) <= dc))
                
                rho_aver = (rho(i) + rho(j)) / 2.;
                
                if (rho_aver > bord_rho(cl(i))) 
                    bord_rho(cl(i)) = rho_aver;
                end
                
                if (rho_aver > bord_rho(cl(j))) 
                    bord_rho(cl(j)) = rho_aver;
                end
        
            end
            
        end
        
    end
    
    for i = 1 : ND
        if (rho(i) < bord_rho(cl(i)))
            halo(i) = 0;
        end
    end
  
end


end

