function [cl,rho,delta]=DPC(dist,dc,ND,NCLUST,label)

%%%%%%%%%%%%%%%%%%%%%%
%  DPC algorithm
% dist matriz de distancia
% ND numero de datos

% density label=1 kernel label=2 cut-off
%label=1;
rho=Density(dist,ND,dc,label);
% distance y vecino mas cercano
[delta,ordrho,nneigh]=DELTA(dist,rho);

% disp('Generated file:DECISION GRAPH')
% disp('column 1:Density')
% disp('column 2:Delta')
% 
% fid = fopen('DECISION_GRAPH', 'w');
% for i=1:ND
%    fprintf(fid, '%6.2f %6.2f\n', rho(i),delta(i));
% end

% selection of centers
%[cl,NCLUST,icl]=ChoiceCentersDPC(rho,delta);
[cl,icl]=ChoiceCenters(rho,delta,NCLUST);

%assignation
for i=1:ND
  if (cl(ordrho(i))==-1)
    cl(ordrho(i))=cl(nneigh(ordrho(i)));
  end
end
%halo
for i=1:ND
  halo(i)=cl(i);
end
if (NCLUST>1)
  for i=1:NCLUST
    bord_rho(i)=0.;
  end
  for i=1:ND-1
    for j=i+1:ND
      if ((cl(i)~=cl(j))&& (dist(i,j)<=dc))
        rho_aver=(rho(i)+rho(j))/2.;
        if (rho_aver>bord_rho(cl(i))) 
          bord_rho(cl(i))=rho_aver;
        end
        if (rho_aver>bord_rho(cl(j))) 
          bord_rho(cl(j))=rho_aver;
        end
      end
    end
  end
  for i=1:ND
    if (rho(i)<bord_rho(cl(i)))
      halo(i)=0;
    end
  end
end
for i=1:NCLUST
  nc=0;
  nh=0;
  for j=1:ND
    if (cl(j)==i) 
      nc=nc+1;
    end
    if (halo(j)==i) 
      nh=nh+1;
    end
  end
  fprintf('CLUSTER: %i CENTER: %i ELEMENTS: %i CORE: %i HALO: %i \n', i,icl(i),nc,nh,nc-nh);
end

% cmap=colormap;
% for i=1:NCLUST
%    ic=int8((i*64.)/(NCLUST*1.));
%    subplot(2,1,1)
%    hold on
%    plot(rho(icl(i)),delta(icl(i)),'o','MarkerSize',8,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
% end
% subplot(2,1,2)
% disp('Performing 2D nonclassical multidimensional scaling')
% Y1 = mdscale(dist, 2, 'criterion','metricstress');
% plot(Y1(:,1),Y1(:,2),'o','MarkerSize',2,'MarkerFaceColor','k','MarkerEdgeColor','k');
% title ('2D Nonclassical multidimensional scaling','FontSize',15.0)
% xlabel ('X')
% ylabel ('Y')
% for i=1:ND
%  A(i,1)=0.;
%  A(i,2)=0.;
% end
% for i=1:NCLUST
%   nn=0;
%   ic=int8((i*64.)/(NCLUST*1.));
%   for j=1:ND
%     if (halo(j)==i)
%       nn=nn+1;
%       A(nn,1)=Y1(j,1);
%       A(nn,2)=Y1(j,2);
%     end
%   end
%   hold on
%   plot(A(1:nn,1),A(1:nn,2),'o','MarkerSize',2,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
% end

%for i=1:ND
%   if (halo(i)>0)
%      ic=int8((halo(i)*64.)/(NCLUST*1.));
%      hold on
%      plot(Y1(i,1),Y1(i,2),'o','MarkerSize',2,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
%   end
%end
% faa = fopen('CLUSTER_ASSIGNATION', 'w');
% disp('Generated file:CLUSTER_ASSIGNATION')
% disp('column 1:element id')
% disp('column 2:cluster assignation without halo control')
% disp('column 3:cluster assignation with halo control')
% for i=1:ND
%    fprintf(faa, '%i %i %i\n',i,cl(i),halo(i));
% end

end