function [cl,icl]=ChoiceCenters(rho,delta,NCLUST)
ND=length(rho);
ind=1:ND;
gamma=rho.*delta;
% rho_norm = (rho - min(rho))/(max(rho) - min(rho));
% delta_norm = (delta - min(delta))/(max(delta) - min(delta));
% gamma=rho_norm.*(delta_norm);

% subplot(2,1,1)
% tt=plot(rho(:),delta(:),'o','MarkerSize',5,'MarkerFaceColor','k','MarkerEdgeColor','k');
% title ('Decision Graph','FontSize',15.0)
% xlabel ('\rho')
% ylabel ('\delta')
%subplot(2,1,1)
%-------------
cl=-ones([1,ND]);
[gamma_ord,ind_gamma_ord]=sort(gamma,'descend');
for k=1:NCLUST
      aux=find(gamma==gamma_ord(k)); 
     icl(k)=aux(1);% peak density point
     cl(icl(k))=k; % cluster label
end
% cmap=colormap;
% for i=1:NCLUST
%    ic=int8((i*64.)/(NCLUST*1.));
%    subplot(2,1,1)
%    hold on
%    plot(rho(icl(i)),delta(icl(i)),'o','MarkerSize',8,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
% end
%subplot(2,1,2)
%plot(gamma_ord,'o','MarkerSize',5,'MarkerFaceColor','k','MarkerEdgeColor','k');
%ylabel ('\gamma')
% for i=1:NCLUST
%    ic=int8((i*64.)/(NCLUST*1.));
%    subplot(2,1,2)
%    hold on
%    aux=find(gamma_ord==gamma(icl(i)));
%    plot(aux(1),gamma(icl(i)),'o','MarkerSize',8,'MarkerFaceColor',cmap(ic,:),'MarkerEdgeColor',cmap(ic,:));
% end

end