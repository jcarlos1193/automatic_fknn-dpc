function purity_value = purity_v2( labels_class, labels_cluster )

    contingency = crosstab(labels_cluster, labels_class);

    num_data_assigned = 0;
    num_all_data = length(labels_cluster);
    
    for idx = 1 : size(contingency, 1)
        [value, index] = max(contingency(idx, :));
        num_data_assigned = num_data_assigned + value;
    end
    
    purity_value = num_data_assigned / num_all_data;

end

