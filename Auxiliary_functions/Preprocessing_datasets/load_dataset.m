function [X, k, labels_class] = load_dataset(name_dataset)

    %%%%% --------------------------- Br. cancer ------------------------ %%%%%

    if strcmp(name_dataset, 'br. cancer')
    
        attr_format = ['%*s', repmat('%f', 1, 9), '%f'];
        filename = 'breast-cancer-wisconsin.data';
        data_csv = preproc_csv(filename , attr_format, '?', NaN, ',', 0);

        k = 2;
        X = data_csv{1}(:, 1:9);
        X = sustituirNaN(X);
        labels_class = data_csv{1}(:, 10);

    %%%%% ------------------------- Ionosphere -------------------------- %%%%%

    elseif strcmp(name_dataset, 'ionosphere')
    
        attr_format = [repmat('%f', 1, 34), '%s'];
        filename = 'ionosphere.data';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 0);

        k = 2;
        X = [data_csv{1}(:, 1), data_csv{1}(:, 3:34)];
        labels_class = data_csv{2};

        labels_class_aux = ones(length(labels_class), 1);
        u_labels_class = unique(labels_class);

        for idx = 1 : length(u_labels_class)
            idx_find = find(strcmp(u_labels_class{idx}, labels_class));
            labels_class_aux(idx_find) = idx;
        end

        labels_class = labels_class_aux;

    %%%%% ------------------------ Opt. digits -------------------------- %%%%%

    elseif strcmp(name_dataset, 'opt. digits')
    
        attr_format = [repmat('%f', 1, 64), '%f'];
        filename = 'optdigits.tra';
        filename2 = 'optdigits.tes';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 0);
        data_csv2 = preproc_csv(filename2 , attr_format, '', NaN, ',', 0);

        k = 10;
        X = [data_csv{1}(:, 1:64); data_csv2{1}(:, 1:64)];
        labels_class = [data_csv{1}(:, 65); data_csv2{1}(:, 65)];

    %%%%% ------------------------ Pen. digits -------------------------- %%%%%

    elseif strcmp(name_dataset, 'pen. digits')
    
        attr_format = [repmat('%f', 1, 16), '%f'];
        filename = 'pendigits.tra';
        filename2 = 'pendigits.tes';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 0);
        data_csv2 = preproc_csv(filename2 , attr_format, '', NaN, ',', 0);

        k = 10;
        X = [data_csv{1}(:, 1:16); data_csv2{1}(:, 1:16)];
        labels_class = [data_csv{1}(:, 17); data_csv2{1}(:, 17)];

    %%%%% ------------------------- Voters -------------------------- %%%%%

    elseif strcmp(name_dataset, 'voters')
    
        attr_format = repmat('%s', 1, 17);
        filename = 'house-votes-84.data';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 0);

        k = 2;
        X = data_csv{1}(:, 2:17);
        labels_class = data_csv{1}(:, 1);

        labels_class_aux = ones(length(labels_class), 1);
        u_labels_class = unique(labels_class);

        for idx = 1 : length(u_labels_class)
            idx_find = find(strcmp(u_labels_class{idx}, labels_class));
            labels_class_aux(idx_find) = idx;
        end

        X_aux = ones(size(X, 1), size(X, 2));
        u_X_aux = unique( X(:, 1) );

        for idx_col = 1 : size(X, 2)

            for idx = 1 : length(u_X_aux)

                value_to_assign = idx;

                if u_X_aux{idx} == '?'
                    value_to_assign = find(strcmp('n', u_X_aux));
                end

                idx_find = find(strcmp(u_X_aux{idx}, X(:, idx_col)));
                X_aux(idx_find, idx_col) = value_to_assign;

            end

        end

        X = X_aux;
        labels_class = labels_class_aux;
        
    %%%%% --------------------------- Image Seg. ------------------------ %%%%%

    elseif strcmp(name_dataset, 'image seg.')
    
        attr_format = [repmat('%f', 1, 19), '%d'];
        filename = 'image-segment.dat';
        data_csv = preproc_csv(filename , attr_format, '', NaN, '\b', 0);

        k = 7;
        X = data_csv{1};
        labels_class = data_csv{2};

    %%%%% ------------------------- Satellite --------------------------- %%%%%

    elseif strcmp(name_dataset, 'satellite')
    
        attr_format = repmat('%f', 1, 37);
        filename = 'sat.trn';
        filename2 = 'sat.tst';
        data_csv = preproc_csv(filename , attr_format, '', NaN, '\b', 0);
        data_csv2 = preproc_csv(filename2 , attr_format, '', NaN, '\b', 0);

        k = 6;
        X = [data_csv{1}(:, 1:36); data_csv2{1}(:, 1:36)];
        labels_class = [data_csv{1}(:, 37); data_csv2{1}(:, 37)];

    %%%%% ---------------------------- Chart ---------------------------- %%%%%

    elseif strcmp(name_dataset, 'chart')
    
        attr_format = repmat('%f', 1, 60);
        filename = 'synthetic_control.data';
        data_csv = preproc_csv(filename , attr_format, '', NaN, '\b', 0);

        k = 6;
        X = data_csv{1};

        labels_class = [];
        labels_class(1:100) = 1;
        labels_class(101:200) = 2;
        labels_class(201:300) = 3;
        labels_class(301:400) = 4;
        labels_class(401:500) = 5;
        labels_class(501:600) = 6;
        labels_class = labels_class';

    %%%%% ------------------------- Smartphone -------------------------- %%%%%

    elseif strcmp(name_dataset, 'smartphone')
    
        attr_format = repmat('%f', 1, 561);
        filename = 'X_train.txt';
        filename2 = 'X_test.txt';
        data_csv = preproc_csv(filename , attr_format, '', NaN, '\b', 0);
        data_csv2 = preproc_csv(filename2 , attr_format, '', NaN, '\b', 0);
        X = [data_csv{1}; data_csv2{1}];

        k = 12;

        attr_format = '%f';
        filename3 = 'y_train.txt';
        filename4 = 'y_test.txt';
        data_csv3 = preproc_csv(filename3 , attr_format, '', NaN, '\b', 0);
        data_csv4 = preproc_csv(filename4 , attr_format, '', NaN, '\b', 0);
        labels_class = [data_csv3{1}; data_csv4{1}];

    %%%%% -------------------------- soybean ---------------------------- %%%%%

    elseif strcmp(name_dataset, 'soybean')
    
        attr_format = ['%s', repmat('%f', 1, 35)];
        filename = 'soybean-large.data';
        filename2 = 'soybean-large.test';
        data_csv = preproc_csv(filename , attr_format, '?', NaN, ',', 0);
        data_csv2 = preproc_csv(filename2 , attr_format, '?', NaN, ',', 0);

        k = 19;
        X = [data_csv{2}; data_csv2{2}];
        X = sustituirNaN(X);
        labels_class = [data_csv{1}; data_csv2{1}];

        labels_class_aux = ones(length(labels_class), 1);
        u_labels_class = unique(labels_class);

        for idx = 1 : length(u_labels_class)
            idx_find = find(strcmp(u_labels_class{idx}, labels_class));
            labels_class_aux(idx_find) = idx;
        end

        labels_class = labels_class_aux;
        
    %%%%% --------------------------- Dermatology ------------------------ %%%%%

    elseif strcmp(name_dataset, 'dermatology')
    
        attr_format = repmat('%f', 1, 35);
        filename = 'dermatology.data';
        data_csv = preproc_csv(filename , attr_format, '?', NaN, ',', 0);

        k = 6;
        X = data_csv{1}(:, 1:33);
        X = sustituirNaN(X);
        labels_class = data_csv{1}(:, 35);

    %%%%% ----------------------------- Glass --------------------------- %%%%%

    elseif strcmp(name_dataset, 'glass')
    
        attr_format = ['%*s', repmat('%f', 1, 10)];
        filename = 'glass.data';
        data_csv = preproc_csv(filename , attr_format, '?', NaN, ',', 0);

        k = 6;
        X = data_csv{1}(:, 1:9);
        X = sustituirNaN(X);
        labels_class = data_csv{1}(:, 10);

    %%%%% -------------------------- isolet ----------------------------- %%%%%

    elseif strcmp(name_dataset, 'isolet')
    
        attr_format = [repmat('%f', 1, 617), '%s'];
        filename = 'isolet1+2+3+4.data';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 0);

        k = 26;
        X = data_csv{1};
        labels_class = regexprep(data_csv{2}, '.$', '', 'lineanchors');
        labels_class_aux = ones(length(labels_class), 1);

        for idx = 1 : length(labels_class)
            labels_class_aux(idx) = str2double(labels_class{idx});
        end

        labels_class = labels_class_aux;
        
    %%%%% ----------------------- parkinsons ---------------------------- %%%%%

    elseif strcmp(name_dataset, 'parkinsons')
    
        attr_format = ['%*s', repmat('%f', 1, 23)];
        filename = 'parkinsons.data';
        data_csv = preproc_csv(filename , attr_format, '', NaN, ',', 1);

        k = 2;
        X = horzcat(data_csv{1}(:, 1:16), data_csv{1}(:, 18:23));
        labels_class = data_csv{1}(:, 17);

    %%%%% --------------------- internet ads. --------------------------- %%%%%

    elseif strcmp(name_dataset, 'internet ads')
    
        attr_format = [repmat('%f', 1, 1558), '%s'];
        filename = 'ad.data';
        data_csv = preproc_csv(filename , attr_format, '?', NaN, ',', 0);

        k = 2;
        X = data_csv{1};
        X = sustituirNaN(X);
        labels_class = data_csv{2};
        u_labels_class = unique(labels_class);

        for idx = 1 : length(u_labels_class)
            idx_find = find(strcmp(u_labels_class{idx}, labels_class));
            labels_class_aux(idx_find) = idx;
        end

        labels_class = labels_class_aux;

    %%%%% ------------------------------ faces -------------------------- %%%%%

    elseif strcmp(name_dataset, 'faces')
    
        load('yale_facedatabase_B.mat');

        k = 10;
        X = double(bigMatrix);
        labels_class = double(trueLabel);

    end

end