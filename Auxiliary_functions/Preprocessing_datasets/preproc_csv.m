function M = preproc_csv(str_filename, attr_format, treatempty, emptyvalue, delimiter, headerlines)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Load data from a csv file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f_csv = fopen(str_filename);
M = textscan(f_csv, attr_format, 'delimiter', delimiter, 'CollectOutput', 1, 'TreatAsEmpty', treatempty, 'EmptyValue', emptyvalue, 'HeaderLines', headerlines);
fclose(f_csv);

end