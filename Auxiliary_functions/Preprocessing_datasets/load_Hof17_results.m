function [ table_purity, table_vmeasure, table_run_time ] = load_Hof17_results

Datasets = load_name_datasets();

NCutH = [96.85; 71.23; 78.71; 77.98; 84.6; 62.19; 74; 66.67; 68.81; 80.79; 96.17; 54.21; 51.67; 75.38; 90.79; 79.83];
NCutH0 = [96.85; 71.23; 79.69; 76.83; 84.37; 63.79; 75.76; 83.83; 72.47; 67.45; 85.52; 58.88; 59.17; 75.38; 89.54; 91.35];
iSVR = [90.41; 70.37; 75.99; 78.25; 81.15; 64.62; 68.33; 72; NaN; 71.85; 80.33; 51.4; NaN; 75.38; 86; NaN];
dePDDP = [96.71; 68.95; 26.43; 56.73; 85.06; 29.32; 75.01; 68; 39.06; 60.56; 85.52; 48.13; 21.88; 75.38; 86.34; 35.91];
Kmeans = [95.42; 70.66; 63.28; 72.23; 84.6; 59.95; 74.11; 76.33; 63.81; 67.74; 86.07; 54.21; 60.05; 75.38; 86.95; 73.73];
BisKm = [95.42; 70.66; 64.54; 72.09; 84.6; 58.55; 73.92; 66.67; 64.51; 78.01; 86.07; 53.27; 52.07; 75.38; 86.95; 62.02];
SCn = [97.28; 72.93; 48.34; 41.71; 84.27; 55.91; 68.75; 78.17; 50.14; 72.87; 94.54; 51.4; 33.71; 75.38; 86.09; 59.74];

table_purity = table(Datasets, NCutH, NCutH0, iSVR, dePDDP, Kmeans, BisKm, SCn);

NCutH = [78.8; 13.49; 71.62; 70.95; 42.82; 59.38; 59.63; 79.65; 61.22; 80.29; 93.56; 31.58; 65.34; 21.96; 33.18; 87.35];
NCutH0 = [78.68; 13.49; 74.65; 73.07; 42.39; 63.81; 62.68; 77.05; 60.04; 71.87; 83.82; 30.85; 70.36; 21.96; 37.05; 92.18];
iSVR = [55.34; 12.64; 71.49; 72.53; 33.59; 62.61; 54.61; 66.41; NaN; 75.38; 76.84; 27.55; NaN; 6.38; 1.85; NaN];
dePDDP = [77.99; 9.82; 28.52; 59.15; 39.25; 35.15; 61.05; 77.75; 51.03; 67.68; 87.39; 30.17; 42.34; 1.78; 3.1; 37.55];
Kmeans = [71.59; 12.5; 61.3; 68.93; 42.3; 59.87; 61.3; 77.43; 56.95; 74.13; 86.33; 31.46; 71.67; 12.42; 8.23; 75.16];
BisKm = [71.59; 12.5; 61.68; 65.71; 42.3; 58.63; 60.15; 76.07; 56.25; 80.19; 86; 31.18; 63.74; 12.42; 8.23; 63.26];
SCn = [82.45; 20.67; 50.01; 48.63; 40.48; 47.9; 53.3; 82.36; 50.3; 73.46; 90.29; 28.79; 51.94; 1.2; 1.59; 72.43];

table_vmeasure = table(Datasets, NCutH, NCutH0, iSVR, dePDDP, Kmeans, BisKm, SCn);

NCutH = [0.01; 0.01; 4.4; 1.55; 0.02; 0.39; 0.81; 0.2; 81.04; 0.47; 0.13; 0.06; 80.86; 0.01; 38.31; 125.1];
NCutH0 = [0.23; 0.16; 12.01; 6.37; 0.16; 1.22; 3.26; 0.78; 217.38; 1.48; 0.56; 0.31; 220.19; 0.1; 324.22; 308.32];
iSVR = [1.81; 2.47; 496.14; 347.77; 1.09; 18.19; 203.3; 14.32; NaN; 12.21; 1.17; 0.32; NaN; 0.39; 3617.97; NaN];
dePDDP = [0.01; 0.02; 0.19; 0.12; 0.01; 0.06; 0.11; 0.12; 14.71; 0.15; 0.06; 0.02; 16.43; 0.01; 5.38; 19.2];
Kmeans = [0.05; 0.03; 1.97; 0.79; 0.03; 0.17; 0.95; 0.11; 82.29; 0.08; 0.03; 0.00; 54.06; 0.01; 9.55; 82.57];
BisKm = [0.05; 0.05; 4.04; 2.35; 0.03; 0.52; 2.54; 0.4; 103.36; 0.29; 0.14; 0.03; 77.95; 0.01; 9.55; 114.05];
SCn = [0.67; 0.2; 28.37; 24.29; 0.27; 6.15; 21.67; 1.61; 1915.1; 0.19; 0.31; 0.08; 1511.53; 0.05; 1402.8; 1786.65];

table_run_time = table(Datasets, NCutH, NCutH0, iSVR, dePDDP, Kmeans, BisKm, SCn);

end

