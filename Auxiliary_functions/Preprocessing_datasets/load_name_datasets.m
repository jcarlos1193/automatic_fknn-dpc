function name_datasets = load_name_datasets

name_datasets = {'br. cancer';
                 'ionosphere';
                 'opt. digits';
                 'pen. digits';
                 'voters';
                 'image seg.';
                 'satellite';
                 'chart';
                 'smartphone';
                 'soybean';
                 'dermatology';
                 'glass';
                 'isolet';
                 'parkinsons';
                 'internet ads';
                 'faces'};

end

