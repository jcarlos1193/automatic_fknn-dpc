function X = sustituirNaN( X )

    for idx = 1 : size(X,2)
        rows = find(isnan(X(:,idx)));
        X(rows,idx) = mean(X(:,idx),'omitnan');
    end


end

