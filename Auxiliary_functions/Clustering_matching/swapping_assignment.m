function final_vector = swapping_assignment( initial_vector, assignment )

    final_vector = initial_vector;

    for idx = 1 : length(assignment)
        
        val_ass = assignment(idx); 
        
        for idx2 = 1 : length(initial_vector)
           
            if initial_vector(idx2) == idx
                final_vector(idx2) = val_ass;
            end
            
        end
        
    end

end

