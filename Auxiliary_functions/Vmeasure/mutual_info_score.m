function value_mis = mutual_info_score( labels_class, labels_cluster )

    contingency = crosstab(labels_class, labels_cluster);
    
    if size(contingency, 2) == 1 && size(contingency, 1) > 1
       contingency = contingency'; 
    end
    
    [nzx, nzy] = find(contingency ~= 0);
    nz_val = nonzeros(contingency);
    
    contingency_sum = sum(sum(contingency));
    pi = sum(contingency, 2)';
    pj = sum(contingency, 1);
    log_contingency_nm = log(nz_val);
    contingency_nm = nz_val / contingency_sum;

    outer = pi(nzx) .* pj(nzy);
    log_outer = -log(outer) + log(sum(pi)) + log(sum(pj));
    
    mis = (contingency_nm' .* (log_contingency_nm' - log(contingency_sum)) + contingency_nm' .* log_outer);
    value_mis = sum(mis);
    
end

