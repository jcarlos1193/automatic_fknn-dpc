function value_entropy = entropy( labels )

    value_entropy = 0;

    if isempty(labels)
       value_entropy = 1.0;
       return;
    end

    unique_labels = unique(labels);
    ocurrences_labels = zeros(1, length(unique_labels));
    
    for idx = 1 : length(unique_labels)
        ocurrences_labels(idx) = length(find(labels == unique_labels(idx)));
    end
    
    sum_ocurrences = sum(ocurrences_labels);
    
    for idx = 1 : length(ocurrences_labels)
        aux = (ocurrences_labels(idx) / sum_ocurrences) * ( log(ocurrences_labels(idx)) - log(sum_ocurrences) );
        value_entropy = value_entropy + aux; 
    end
    
    value_entropy = -value_entropy;
    
end

