function [ homogeneity, completeness, v_measure ] = homogeneity_completeness_v_measure( labels_class, labels_cluster )

    entropy_C = entropy(labels_class);
    entropy_K = entropy(labels_cluster);
    MI = mutual_info_score(labels_class, labels_cluster);

    homogeneity = 1;
    completeness = 1;
    v_measure = 0;
    
    if entropy_C > 0
        homogeneity = MI / (entropy_C);
    end
    
    if entropy_K > 0
        completeness = MI / (entropy_K);
    end
    
    if homogeneity + completeness > 0
        v_measure = (2.0 * homogeneity * completeness / (homogeneity + completeness));
    end
    
end

