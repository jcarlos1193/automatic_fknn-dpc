# Automatic FKNN-DPC repository

Research on density peak clustering methods: an automatic parameters-tunning approach and optimal rules for parameter/center selection. In this repository, we include the implementation of DPC, FKNN-DPC and their improved versions so that the user can carry out a computational experience using the same datasets as the paper has. In addition, the repository has a directory where tables and figures included in the research can be generated on your own.

To evaluate any algorithm taken into account in this implementation, the user can run the script called *main_ALL_DPC.m*. To organise the code we have created the following directories:

- **Auxiliary_functions**: In this directory the user can find functions to evaluate quality of algorithms through V-Measure and Purity. Moreover, we have included everything necessary to preprocess the datasets from csv files and generate tables using Latex format.

- **Datasets**: This directory is composed of 16 datasets that have been used to evaluate the performance of all algorithms mentioned in the paper.

- **DPC_functions**: The functions related to DPC and FKNN-DPC with their multiple versions are included in this directory. Others carry out specific tasks such as strategies that assign remaining points to corresponding clusters or computation of local densities.

- **Tables_Plots_Paper**: To generate the tables with the comparison between the state-of-the-art algorithms and DPC, we have uploaded the scripts *reportResultsClusteval.m* and *reportResultsOtherPapers.m* to this directory. Furthermore, the code *build_tables.m* generates tables with the results of all algorithms according to V-Measure and Purity metrics. The figure related to the decision graph in which DPC and their improved version are compared is obtained by using the script *Plot_decision_graph_DPC.m*. Regarding the boxplot figure, the code *spearman_and_boxplot_regret.m* can be used.  

