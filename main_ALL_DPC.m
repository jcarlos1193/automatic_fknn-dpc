clear;
clc;
close;

addpath(genpath('.\Auxiliary_functions'));
addpath(genpath('.\Datasets'));
addpath(genpath('.\DPC_Functions'))

name_datasets = load_name_datasets();

[ table_purity, table_vmeasure, table_run_time ] = load_Hof17_results;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% --------------- Improved FKNN-DPC Information gain (c*,k*) -----------
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('-------- Improved FKNN-DPC Information gain (c*,k*) -------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_K = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets,1)
    
    fprintf('\n-------------------- %s ----------------------\n\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    [ ~, ~, purity, vmeasure, best_K, cpu_time ] = FKNN_DPC_improved_v4( X, NCLUST, labels_class, name_datasets{idx} );
    
    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_K(idx) = best_K;
    list_cputime(idx) = cpu_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_K, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'K', 'CPU_Time'};

save('Results/RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_FKNN-DPC-Impr-v4.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.FKNN_DPC_Impr_v4 = list_purity * 100;
table_vmeasure.FKNN_DPC_Impr_v4 = list_vmeasure * 100;
table_run_time.FKNN_DPC_Impr_v4 = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ------------ Improved FKNN-DPC Information gain (k*) -----------------
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('---------- Improved FKNN-DPC Information gain (k*) ---------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_K = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets,1)
    
    fprintf('\n-------------------- %s ----------------------\n\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    [ ~, ~, ~, purity, vmeasure, best_K, cpu_time ] = FKNN_DPC_improved_v3( X, NCLUST, labels_class, name_datasets{idx} );
    
    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_K(idx) = best_K;
    list_cputime(idx) = cpu_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_K, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'K', 'CPU_Time'};

save('Results/RESULTS_DATASETS_FKNN-DPC-Impr-v3.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_FKNN-DPC-Impr-v3.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.FKNN_DPC_Impr_v3 = list_purity * 100;
table_vmeasure.FKNN_DPC_Impr_v3 = list_vmeasure * 100;
table_run_time.FKNN_DPC_Impr_v3 = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% --------------- Improved FKNN-DPC V-Measure (c*,k*) ------------------
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('---------------- Improved FKNN-DPC V-Measure (c*,k*) ------------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_K = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets,1)
    
    fprintf('\n-------------------- %s ----------------------\n\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    [ ~, ~, purity, vmeasure, best_K, cpu_time ] = FKNN_DPC_improved_v4_1( X, NCLUST, labels_class, name_datasets{idx} );
    
    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_K(idx) = best_K;
    list_cputime(idx) = cpu_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_K, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'K', 'CPU_Time'};

save('Results/RESULTS_DATASETS_FKNN-DPC-Impr-v4_1.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_FKNN-DPC-Impr.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.FKNN_DPC_Impr_v41 = list_purity * 100;
table_vmeasure.FKNN_DPC_Impr_v41 = list_vmeasure * 100;
table_run_time.FKNN_DPC_Impr_v41 = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ----------------- Improved FKNN-DPC V-Measure (k*) -------------------
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('------------------- Improved FKNN-DPC -------------------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_K = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets,1)
    
    fprintf('\n-------------------- %s ----------------------\n\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    [ ~, ~, ~, purity, vmeasure, best_K, cpu_time ] = FKNN_DPC_improved( X, NCLUST, labels_class, name_datasets{idx} );
    
    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_K(idx) = best_K;
    list_cputime(idx) = cpu_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_K, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'K', 'CPU_Time'};

save('Results/RESULTS_DATASETS_FKNN-DPC-Impr.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_FKNN-DPC-Impr.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.FKNN_DPC_Impr = list_purity * 100;
table_vmeasure.FKNN_DPC_Impr = list_vmeasure * 100;
table_run_time.FKNN_DPC_Impr = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ---------------------- Original FKNN-DPC  --------------------------- 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('---------------------- Original FKNN-DPC  ---------------------------')

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_K = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets, 1)
    
    fprintf('- %s\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    K = round(0.015 * size(X,1));
    
    dist_vector = pdist(X);
    dist = squareform(dist_vector);
    
    timerVal = tic;
    [labels_cluster, ~, ~] = FKNN_DPC(X, NCLUST, K, dist);
    total_time = toc(timerVal);

    purity = purity_v2(labels_class, labels_cluster);
    [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, labels_cluster);

    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_K(idx) = K;
    list_cputime(idx) = total_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_K, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'K', 'CPU_Time'};

save('Results/RESULTS_DATASETS_FKNN-DPC.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_FKNN-DPC.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.FKNN_DPC = list_purity * 100;
table_vmeasure.FKNN_DPC = list_vmeasure * 100;
table_run_time.FKNN_DPC = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ------------------------ Original DPC  ------------------------------ 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('------------------------ Original DPC  ------------------------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets, 1)
    
    fprintf('- %s\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    percent = 2;
    
    dist_vector = pdist(X);
    dist = squareform(dist_vector);
    
    timerVal = tic;
    [labels_cluster, ~, ~] = DPC_Original(X, NCLUST, percent, dist, dist_vector);
    total_time = toc(timerVal);

    purity = purity_v2(labels_class, labels_cluster);
    [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, labels_cluster);

    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_cputime(idx) = total_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'CPU_Time'};

save('Results/RESULTS_DATASETS_DPC.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_DPC.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.DPC = list_purity * 100;
table_vmeasure.DPC = list_vmeasure * 100;
table_run_time.DPC = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ------------------------ Improved DPC  ------------------------------ 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('------------------------ Improved DPC  ------------------------------');

list_purity = zeros(size(name_datasets,1),1);
list_vmeasure = zeros(size(name_datasets,1),1);
list_cputime = zeros(size(name_datasets,1),1);
             
for idx = 1 : size(name_datasets, 1)
    
    fprintf('- %s\n',name_datasets{idx});
    
    [X, NCLUST, labels_class] = load_dataset(name_datasets{idx});
    X = MinMaxNorm(X);
    
    dist_vector = pdist(X);
    dist = squareform(dist_vector);
    
    timerVal = tic;
    [labels_cluster, ~] = DPC_Impr(X, NCLUST, labels_class, dist, dist_vector);
    total_time = toc(timerVal);

    purity = purity_v2(labels_class, labels_cluster);
    [~,~,vmeasure] = homogeneity_completeness_v_measure(labels_class, labels_cluster);

    list_purity(idx) = purity;
    list_vmeasure(idx) = vmeasure;
    list_cputime(idx) = total_time;
    
end
             
T = table(name_datasets, list_purity, list_vmeasure, list_cputime);
T.Properties.VariableNames = {'Dataset', 'Purity', 'V_Measure', 'CPU_Time'};

save('Results/RESULTS_DATASETS_DPC_Impr.mat', 'T');
writetable(T, 'Results/RESULTS_DATASETS_DPC_Impr.xlsx', 'Sheet', 1, 'Range', 'A1');

table_purity.DPC_Impr = list_purity * 100;
table_vmeasure.DPC_Impr = list_vmeasure * 100;
table_run_time.DPC_Impr = list_cputime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ------------------- Write results into a latex file --------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

latextable(table2cell(table_purity), 'Horiz', table_purity.Properties.VariableNames, 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', 'Results/RESULTS_LATEX_purity.tex');
latextable(table2cell(table_vmeasure), 'Horiz', table_vmeasure.Properties.VariableNames, 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', 'Results/RESULTS_LATEX_vmeasure.tex');
latextable(table2cell(table_run_time), 'Horiz', table_run_time.Properties.VariableNames, 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', 'Results/RESULTS_LATEX_cpu_time.tex');

save('Results/RESULTS_purity.mat', 'table_purity');
save('Results/RESULTS_vmeasure.mat', 'table_vmeasure');
save('Results/RESULTS_cpu_time.mat', 'table_run_time');