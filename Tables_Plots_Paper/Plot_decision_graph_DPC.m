clc;
clear;

addpath(genpath('..\Datasets'));
addpath(genpath('..\Auxiliary_functions'));
addpath(genpath('..\DPC_Functions'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Load the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[X, k, labels_class] = load_dataset('ionosphere');
X = MinMaxNorm(X);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Compute optimal dc and cluster centers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[labels_cluster_impr, opt_icl, opt_dc] = DPC_c_k_vm(X, k, labels_class);
[~, icl_best_gamma, ~] = DPC_without_opt(X, k, 'dc', opt_dc);
[labels_cluster_org, icl, ~] = DPC_without_opt(X, k, 'percentage', 2);

purity_impr = purity_v2(labels_class, labels_cluster_impr);
[~,~,vmeasure_impr] = homogeneity_completeness_v_measure(labels_class, labels_cluster_impr);

purity_org = purity_v2(labels_class, labels_cluster_org);
[~,~,vmeasure_org] = homogeneity_completeness_v_measure(labels_class, labels_cluster_org);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Plot points in the decision graph
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ND = size(X,1);
colours = ['r', 'b'];
[rho, delta] = DPC_rho_delta(X, 'dc', opt_dc);

subplot(2,2,1);
xlabel ('\rho')
ylabel ('\delta')
set(gca, 'fontsize', 20);

for idx = 1 : ND
    k_ix = labels_class(idx);
    hold on;
    
    if ismember(idx, icl_best_gamma)
        plot(rho(idx),delta(idx),'o','MarkerSize',15,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix),'Marker','s');
    else
        plot(rho(idx),delta(idx),'o','MarkerSize',5,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix));
    end
end

subplot(2,2,3);
xlabel ('\rho')
ylabel ('\delta')
set(gca, 'fontsize', 20);

contingency = crosstab(labels_cluster_impr, labels_class);
[assignment, ~] = munkres(contingency * -1);
labels_cluster_impr = swapping_assignment( labels_cluster_impr, assignment );

for idx = 1 : ND
    k_ix = labels_cluster_impr(idx);
    hold on;
    
    if ismember(idx, opt_icl)
        plot(rho(idx),delta(idx),'o','MarkerSize',15,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix),'Marker','s');
    else
        plot(rho(idx),delta(idx),'o','MarkerSize',5,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix));
    end
end

[rho, delta] = DPC_rho_delta(X, 'percentage', 2);

subplot(2,2,2);
xlabel ('\rho')
ylabel ('\delta')
set(gca, 'fontsize', 20);

for idx = 1 : ND
    k_ix = labels_class(idx);
    hold on;
    
    if ismember(idx, icl)
        plot(rho(idx),delta(idx),'o','MarkerSize',15,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix),'Marker','s');
    else
        plot(rho(idx),delta(idx),'o','MarkerSize',5,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix));
    end
end

subplot(2,2,4);
xlabel ('\rho')
ylabel ('\delta')
set(gca, 'fontsize', 20);

contingency = crosstab(labels_cluster_org, labels_class);
[assignment, ~] = munkres(contingency * -1);
labels_cluster_org = swapping_assignment( labels_cluster_org, assignment );

for idx = 1 : ND
    k_ix = labels_cluster_org(idx);
    hold on;
    
    if ismember(idx, icl)
        plot(rho(idx),delta(idx),'o','MarkerSize',15,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix),'Marker','s');
    else
        plot(rho(idx),delta(idx),'o','MarkerSize',5,'MarkerFaceColor',colours(k_ix),'MarkerEdgeColor',colours(k_ix));
    end
end
