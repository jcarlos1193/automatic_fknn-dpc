%% Table Comparison of FKNN versions with V-Measure metric

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC-Impr.mat')
fknn_k_vm_v1 = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_vmeasure_v4_1\RESULTS_DATASETS_FKNN-DPC-Impr-v4_1.mat')
fknn_c_k_vm_v41 = T.V_Measure .* 100;

load('..\Results\Results_PCA_K_v3\RESULTS_DATASETS_FKNN-DPC-Impr-v3.mat')
fknn_k_ig_v3 = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_ig_v4\RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat')
fknn_c_k_ig_v4 = T.V_Measure .* 100;


%% Spearman correlation

[RHO_k, PVAL_k] = corr(fknn_k_vm_v1, fknn_k_ig_v3, 'Type', 'Spearman');
[RHO_c_k, PVAL_c_k] = corr(fknn_c_k_vm_v41, fknn_c_k_ig_v4, 'Type', 'Spearman');


%% Boxplot V-Measure

load('..\Results\Results_v1\RESULTS_vmeasure.mat')
t_vm = table_vmeasure(:,1:8);

load('..\Results\Results_v1\RESULTS_DATASETS_DPC.mat')
t_vm.DPC = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC.mat')
t_vm.FKNN = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_DPC_Impr.mat')
t_vm.DPC_c_dc = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC-Impr.mat')
t_vm.FKNN_k = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_vmeasure_v4_1\RESULTS_DATASETS_FKNN-DPC-Impr-v4_1.mat')
t_vm.FKNN_c_k = T.V_Measure .* 100;

load('..\Results\Results_PCA_K_v3\RESULTS_DATASETS_FKNN-DPC-Impr-v3.mat')
t_vm.FKNN_k_ig = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_ig_v4\RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat')
t_vm.FKNN_c_k_ig = T.V_Measure .* 100;

t_vm{9,4} = 0.0;
t_vm{13,4} = 0.0;
t_vm{16,4} = 0.0;

box_data = [t_vm.FKNN_c_k, t_vm.FKNN_k, t_vm.FKNN_c_k_ig, t_vm.NCutH, t_vm.DPC_c_dc, t_vm.FKNN_k_ig, t_vm.NCutH0, t_vm.Kmeans, t_vm.BisKm, t_vm.SCn, t_vm.DPC, t_vm.FKNN, t_vm.dePDDP, t_vm.iSVR];
str_algs = {'\texttt{FKNN$(c^*,k^*)$}', '\texttt{FKNN$(k^*)$}', '\texttt{$\widehat{FKNN(c^*,k^*)}$}', '\texttt{NCutH}', '\texttt{DPC$(c^*,d_c^*)$}', '\texttt{$\widehat{FKNN(k^*)}$}', '\texttt{NCutH$_0$}', '\texttt{K-means}', '\texttt{BisKm}', '\texttt{SCn}', '\texttt{DPC}', '\texttt{FKNN}', '\texttt{dePDDP}', '\texttt{iSVR}'};
%str_algs = {'\texttt{FKNN$(c^*,k^*)$}', '\texttt{FKNN$(k^*)$}', '\texttt{aFKNN$(c^*,k^*)$}', '\texttt{NCutH}', '\texttt{DPC$(c^*,d_c^*)$}', '\texttt{aFKNN$(k^*)$}', '\texttt{NCutH$_0$}', '\texttt{K-means}', '\texttt{BisKm}', '\texttt{SCn}', '\texttt{DPC}', '\texttt{FKNN}', '\texttt{dePDDP}', '\texttt{iSVR}'};

boxplot(box_data, str_algs);

ax = gca;
ax.FontSize = 19;
ax.XAxis.TickLabelInterpreter = 'latex';

title('Boxplot')
xlabel('Algorithm')
ylabel('V-Measure')