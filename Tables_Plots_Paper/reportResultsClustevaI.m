% https://clusteval.sdu.dk/1/mains
% https://www.baumbachlab.net/

name_algs = vmcsv.Properties.VariableNames(3:length(vmcsv.Properties.VariableNames));

vm_dpc = vmdpccsv.clusterdp;
f1_dpc = f1dpccsv.clusterdp;

synth_vm_mean = zeros(length(name_algs),1);
real_vm_mean = zeros(length(name_algs),1);
synth_f1_mean = zeros(length(name_algs),1);
real_f1_mean = zeros(length(name_algs),1);

synth_vm_std = zeros(length(name_algs),1);
real_vm_std = zeros(length(name_algs),1);
synth_f1_std = zeros(length(name_algs),1);
real_f1_std = zeros(length(name_algs),1);


for idx = 1 : length(name_algs)
    
    vm_alg = vmcsv{vmcsv.DataType == 'Synthetic', name_algs{idx}};
    ARI = vm_alg - vm_dpc(vmdpccsv.DataType == 'Synthetic');
    synth_vm_mean(idx) = mean(ARI) * 100;
    synth_vm_std(idx) = std(ARI) * 100;
    
    vm_alg = vmcsv{vmcsv.DataType ~= 'Synthetic', name_algs{idx}};
    ARI = vm_alg - vm_dpc(vmdpccsv.DataType ~= 'Synthetic');
    real_vm_mean(idx) = mean(ARI) * 100;
    real_vm_std(idx) = std(ARI) * 100;
    
    f1_alg = f1csv{f1csv.DataType == 'Synthetic', name_algs{idx}};
    ARI = f1_alg - f1_dpc(f1dpccsv.DataType == 'Synthetic');
    synth_f1_mean(idx) = mean(ARI) * 100;
    synth_f1_std(idx) = std(ARI) * 100;
    
    f1_alg = f1csv{f1csv.DataType ~= 'Synthetic', name_algs{idx}};
    ARI = f1_alg - f1_dpc(f1dpccsv.DataType ~= 'Synthetic');
    real_f1_mean(idx) = mean(ARI) * 100;
    real_f1_std(idx) = std(ARI) * 100;
    
end

comparison_table = table(synth_vm_mean, synth_vm_std, real_vm_mean, real_vm_std, synth_f1_mean, synth_f1_std, real_f1_mean, real_f1_std, 'RowNames', name_algs);


%% OUTPUT DATA TO FILE

fid = fopen('results_clusteval.tex','w'); 

for idx = 1 : length(name_algs)

    fprintf(fid,'\\multirow{2}{*}{\\shortstack[1]{\\texttt{%s}}} & %.2f & %.2f & %.2f & %.2f & \\\\\n',name_algs{idx}, comparison_table{idx,1}, comparison_table{idx,2}, comparison_table{idx,3}, comparison_table{idx,4});
    fprintf(fid,' & %.2f & %.2f & %.2f & %.2f & \\\\\\cline{1-5}\n', comparison_table{idx,5}, comparison_table{idx,6}, comparison_table{idx,7}, comparison_table{idx,8});

end

fclose(fid);