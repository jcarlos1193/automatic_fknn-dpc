
[ table_snn_synth, table_snn_real, table_cdp_real, table_alg_dcf_real, table_fknn_synth, table_fknn_real ] = load_paper_results;


%% SNN, Synthetic datasets (9/S/..)

snn_ami_synth = table_snn_synth{:, 'snn_dpc_ami'} - table_snn_synth{:, 'dpc_ami'};
snn_ami_synth_mean = mean(snn_ami_synth) * 100;
snn_ami_synth_std = std(snn_ami_synth) * 100;

snn_ari_synth = table_snn_synth{:, 'snn_dpc_ari'} - table_snn_synth{:, 'dpc_ari'};
snn_ari_synth_mean = mean(snn_ari_synth) * 100;
snn_ari_synth_std = std(snn_ari_synth) * 100;

snn_fmi_synth = table_snn_synth{:, 'snn_dpc_fmi'} - table_snn_synth{:, 'dpc_fmi'};
snn_fmi_synth_mean = mean(snn_fmi_synth) * 100;
snn_fmi_synth_std = std(snn_fmi_synth) * 100;


%% SNN, Real datasets (15/RO/..)

snn_ami_real = table_snn_real{:, 'snn_dpc_ami'} - table_snn_real{:, 'dpc_ami'};
snn_ami_real_mean = mean(snn_ami_real) * 100;
snn_ami_real_std = std(snn_ami_real) * 100;

snn_ari_real = table_snn_real{:, 'snn_dpc_ari'} - table_snn_real{:, 'dpc_ari'};
snn_ari_real_mean = mean(snn_ari_real) * 100;
snn_ari_real_std = std(snn_ari_real) * 100;

snn_fmi_real = table_snn_real{:, 'snn_dpc_fmi'} - table_snn_real{:, 'dpc_fmi'};
snn_fmi_real_mean = mean(snn_fmi_real) * 100;
snn_fmi_real_std = std(snn_fmi_real) * 100;


%% CDP, real datasets (17/R/..)

cdp_acc = table_cdp_real{:, 'cdp_acc'} - table_cdp_real{:, 'dpc_acc'};
cdp_acc_mean = mean(cdp_acc) * 100;
cdp_acc_std = std(cdp_acc) * 100;

cdp_nmi = table_cdp_real{:, 'cdp_nmi'} - table_cdp_real{:, 'dpc_nmi'};
cdp_nmi_mean = mean(cdp_nmi) * 100;
cdp_nmi_std = std(cdp_nmi) * 100;

cdp_f1 = table_cdp_real{:, 'cdp_f1'} - table_cdp_real{:, 'dpc_f1'};
cdp_f1_mean = mean(cdp_f1) * 100;
cdp_f1_std = std(cdp_f1) * 100;

cdp_ari = table_cdp_real{:, 'cdp_ari'} - table_cdp_real{:, 'dpc_ari'};
cdp_ari_mean = mean(cdp_ari) * 100;
cdp_ari_std = std(cdp_ari) * 100;


%% Alg based on DCF, real datasets (11/RO/..)

alg_dcf_f1 = table_alg_dcf_real{:, 'alg_dcf_f1'} - table_alg_dcf_real{:, 'dpc_f1'};
alg_dcf_f1_mean = mean(alg_dcf_f1) * 100;
alg_dcf_f1_std = std(alg_dcf_f1) * 100;

alg_dcf_ri = table_alg_dcf_real{:, 'alg_dcf_ri'} - table_alg_dcf_real{:, 'dpc_ri'};
alg_dcf_ri_mean = mean(alg_dcf_ri) * 100;
alg_dcf_ri_std = std(alg_dcf_ri) * 100;


%% FKNN-DPC, synthetic datasets (6/S/..)

fknn_acc_synth = table_fknn_synth{:, 'fknn_dpc_acc'} - table_fknn_synth{:, 'dpc_acc'};
fknn_acc_synth_mean = mean(fknn_acc_synth) * 100;
fknn_acc_synth_std = std(fknn_acc_synth) * 100;

fknn_ami_synth = table_fknn_synth{:, 'fknn_dpc_ami'} - table_fknn_synth{:, 'dpc_ami'};
fknn_ami_synth_mean = mean(fknn_ami_synth) * 100;
fknn_ami_synth_std = std(fknn_ami_synth) * 100;

fknn_ari_synth = table_fknn_synth{:, 'fknn_dpc_ari'} - table_fknn_synth{:, 'dpc_ari'};
fknn_ari_synth_mean = mean(fknn_ari_synth) * 100;
fknn_ari_synth_std = std(fknn_ari_synth) * 100;


%% FKNN-DPC, real datasets (13/RO/..)

fknn_acc_real = table_fknn_real{:, 'fknn_dpc_acc'} - table_fknn_real{:, 'dpc_acc'};
fknn_acc_real_mean = mean(fknn_acc_real) * 100;
fknn_acc_real_std = std(fknn_acc_real) * 100;

fknn_ami_real = table_fknn_real{:, 'fknn_dpc_ami'} - table_fknn_real{:, 'dpc_ami'};
fknn_ami_real_mean = mean(fknn_ami_real) * 100;
fknn_ami_real_std = std(fknn_ami_real) * 100;

fknn_ari_real = table_fknn_real{:, 'fknn_dpc_ari'} - table_fknn_real{:, 'dpc_ari'};
fknn_ari_real_mean = mean(fknn_ari_real) * 100;
fknn_ari_real_std = std(fknn_ari_real) * 100;


%% NCutH, real datasets (16/R/..)

load('..\Results\Results_v1\RESULTS_vmeasure.mat')
load('..\Results\Results_v1\RESULTS_purity.mat')

ncuth_vm_real = table_vmeasure{:, 'NCutH'} - table_vmeasure{:, 'DPC'};
ncuth_vm_real_mean = mean(ncuth_vm_real);
ncuth_vm_real_std = std(ncuth_vm_real);

ncuth_pur_real = table_purity{:, 'NCutH'} - table_purity{:, 'DPC'};
ncuth_pur_real_mean = mean(ncuth_pur_real);
ncuth_pur_real_std = std(ncuth_pur_real);


%% NCutH0, real datasets (16/R/..)

ncuth0_vm_real = table_vmeasure{:, 'NCutH0'} - table_vmeasure{:, 'DPC'};
ncuth0_vm_real_mean = mean(ncuth0_vm_real);
ncuth0_vm_real_std = std(ncuth0_vm_real);

ncuth0_pur_real = table_purity{:, 'NCutH0'} - table_purity{:, 'DPC'};
ncuth0_pur_real_mean = mean(ncuth0_pur_real);
ncuth0_pur_real_std = std(ncuth0_pur_real);


%% FKNN(c*,k*)_ig, real datasets (16/R/..)

load('..\Results\Results_centroides_pca_k_ig_v4\RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat')
FKNN_c_k_ig_vm = T.V_Measure .* 100;
FKNN_c_k_ig_pur = T.Purity .* 100;

fknn_ig_vm_real = FKNN_c_k_ig_vm - table_vmeasure{:, 'DPC'};
fknn_ig_vm_real_mean = mean(fknn_ig_vm_real);
fknn_ig_vm_real_std = std(fknn_ig_vm_real);

fknn_ig_pur_real = FKNN_c_k_ig_pur - table_purity{:, 'DPC'};
fknn_ig_pur_real_mean = mean(fknn_ig_pur_real);
fknn_ig_pur_real_std = std(fknn_ig_pur_real);


%% 7 - OUTPUT DATA TO FILE

fid = fopen('results_papers.tex','w'); 

fprintf(fid,'\\multirow{3}{*}{\\texttt{SNN}} & %.2f & %.2f & %.2f & %.2f & 9/S/AMI 15/RO/AMI \\\\\n', snn_ami_synth_mean, snn_ami_synth_std, snn_ami_real_mean, snn_ami_real_std);
fprintf(fid,' & %.2f & %.2f & %.2f & %.2f & 9/S/ARI 15/RO/ARI \\\\\n', snn_ari_synth_mean, snn_ari_synth_std, snn_ari_real_mean, snn_ari_real_std);
fprintf(fid,' & %.2f & %.2f & %.2f & %.2f & 9/S/FMI 15/RO/FMI \\\\\\hline\n', snn_fmi_synth_mean, snn_fmi_synth_std, snn_fmi_real_mean, snn_fmi_real_std);

fprintf(fid,'\\multirow{4}{*}{\\texttt{CDP}} & - & - & %.2f & %.2f & 17/R/ACC \\\\\n', cdp_acc_mean, cdp_acc_std);
fprintf(fid,' & - & - & %.2f & %.2f & 17/R/NMI \\\\\n', cdp_nmi_mean, cdp_nmi_std);
fprintf(fid,' & - & - & %.2f & %.2f & 17/R/F1 \\\\\n', cdp_f1_mean, cdp_f1_std);
fprintf(fid,' & - & - & %.2f & %.2f & 17/R/ARI \\\\\\hline\n', cdp_ari_mean, cdp_ari_std);

fprintf(fid,'\\multirow{2}{*}{DCF-based alg.} & - & - & %.2f & %.2f & 11/RO/F1 \\\\\n', alg_dcf_f1_mean, alg_dcf_f1_std);
fprintf(fid,' & - & - & %.2f & %.2f & 11/RO/RI \\\\\\hline\n', alg_dcf_ri_mean, alg_dcf_ri_std);

fprintf(fid,'\\multirow{3}{*}{\\texttt{FKNN-DPC}} & %.2f & %.2f & %.2f & %.2f & 6/S/ACC 13/RO/ACC \\\\\n', fknn_acc_synth_mean, fknn_acc_synth_std, fknn_acc_real_mean, fknn_acc_real_std);
fprintf(fid,' & %.2f & %.2f & %.2f & %.2f & 6/S/AMI 13/RO/AMI \\\\\n', fknn_ami_synth_mean, fknn_ami_synth_std, fknn_ami_real_mean, fknn_ami_real_std);
fprintf(fid,' & %.2f & %.2f & %.2f & %.2f & 6/S/ARI 13/RO/ARI \\\\\\hline\n', fknn_ari_synth_mean, fknn_ari_synth_std, fknn_ari_real_mean, fknn_ari_real_std);

fprintf(fid,'\\multirow{2}{*}{\\texttt{NCutH}} & - & - & %.2f & %.2f & 0/16/VM \\\\\n', ncuth_vm_real_mean, ncuth_vm_real_std);
fprintf(fid,' & - & - & %.2f & %.2f & 0/16/PUR \\\\\\hline\n', ncuth_pur_real_mean, ncuth_pur_real_std);

fprintf(fid,'\\multirow{2}{*}{\\texttt{NCutH$_0$}} & - & - & %.2f & %.2f & 0/16/VM \\\\\n', ncuth0_vm_real_mean, ncuth0_vm_real_std);
fprintf(fid,' & - & - & %.2f & %.2f & 0/16/PUR \\\\\\hline\n', ncuth0_pur_real_mean, ncuth0_pur_real_std);

fprintf(fid,'\\multirow{2}{*}{\\texttt{$\\widehat{FKNN(c^*,k^*)}$}} & - & - & %.2f & %.2f & 0/16/VM \\\\\n', fknn_ig_vm_real_mean, fknn_ig_vm_real_std);
fprintf(fid,' & - & - & %.2f & %.2f & 0/16/PUR \\\\\\hline\n', fknn_ig_pur_real_mean, fknn_ig_pur_real_std);

fclose(fid);
