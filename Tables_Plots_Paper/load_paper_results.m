function [ table_snn_synth, table_snn_real, table_cdp_real, table_alg_dcf_real, table_fknn_synth, table_fknn_real ] = load_paper_results

%% SNN - DPC, synthetic (9/S/..)

snn_dpc_ami = [0.95; 0.8975; 1; 0.9001; 0.9938; 1; 0.9642; 1; 0.9386];
snn_dpc_ari = [0.9594; 0.9502; 1; 0.9294; 0.9928; 1; 0.9509; 1; 0.9264];
snn_dpc_fmi = [0.9681; 0.9768; 1; 0.9529; 0.9933; 1; 0.9525; 1; 0.9313];

dpc_ami = [1; 1; 0.6183; 0.5212; 0.9938; 1; 0.9554; 1; 0.9437];
dpc_ari = [1; 1; 0.7146; 0.4717; 0.9928; 1; 0.9365; 1; 0.9352];
dpc_fmi = [1; 1; 0.8819; 0.6664; 0.9933; 1; 0.9385; 1; 0.9395];

table_snn_synth = table(snn_dpc_ami, snn_dpc_ari, snn_dpc_fmi, dpc_ami, dpc_ari, dpc_fmi);


%% SNN - DPC, real dataset (15/RO/..)

snn_dpc_ami = [0.9124; 0.8735; 0.7520; 0.7509; 0.6725; 0.5834; 0.3644; 0.3984; 0.3259; 0.6711; 0.8749; 0.1529; 0.4082; 0.4033; 0.8244];
snn_dpc_ari = [0.9222; 0.8992; 0.8503; 0.7890; 0.5770; 0.3927; 0.4949; 0.4176; 0.3108; 0.7547; 0.8689; 0.2916; 0.5716; 0.2377; 0.7201];
snn_dpc_fmi = [0.9479; 0.9330; 0.9305; 0.8589; 0.6457; 0.4507; 0.7798; 0.6164; 0.6049; 0.8243; 0.9021; 0.8032; 0.7731; 0.2879; 0.7284];

dpc_ami = [0.8606; 0.7065; 0.0007; 0.7299; 0.6927; 0.5358; 0.1504; 0.3261; 0.0896; 0.4978; 0.7840; 0.2478; 0.1154; 0.3226; 0.8259];
dpc_ari = [0.8857; 0.6724; -0.0028; 0.7670; 0.6004; 0.3193; 0.2357; 0.2698; 0.0695; 0.4465; 0.7760; 0.1256; 0.1394; 0.2097; 0.6863];
dpc_fmi = [0.9233; 0.7835; 0.7257; 0.8444; 0.6730; 0.3717; 0.6491; 0.5292; 0.4580; 0.5775; 0.8221; 0.6187; 0.5024; 0.2601; 0.6993];

table_snn_real = table(snn_dpc_ami, snn_dpc_ari, snn_dpc_fmi, dpc_ami, dpc_ari, dpc_fmi);


%% CDP, real dataset (17/R/..)

cdp_acc = [0.93; 0.79; 0.62; 0.59; 0.24; 0.3; 0.51; 0.45; 0.87; 0.53; 0.53; 0.34; 0.5; 0.79; 0.65; 0.78; 0.38];
cdp_nmi = [0.9; 0.76; 0.79; 0.65; 0.45; 0.38; 0.76; 0.13; 0.62; 0.69; 0.76; 0.63; 0.75; 0.51; 0.25; 0.15; 0.32];
cdp_f1 = [0.88; 0.69; 0.47; 0.58; 0.13; 0.18; 0.39; 0.39; 0.77; 0.41; 0.41; 0.21; 0.37; 0.67; 0.58; 0.8; 0.29];
cdp_ari = [0.87; 0.66; 0.46; 0.52; 0.12; 0.14; 0.39; 0.1; 0.65; 0.39; 0.4; 0.2; 0.36; 0.52; 0.3; 0.37; 0.17];

dpc_acc = [0.6; 0.75; 0.52; 0.34; 0.22; 0.27; 0.39; 0.4; 0.35; 0.4; 0.25; 0.15; 0.47; 0.41; 0.61; 0.77; 0.31];
dpc_nmi = [0.68; 0.72; 0.72; 0.29; 0.42; 0.37; 0.73; 0.076; 0.014; 0.57; 0.58; 0.41; 0.73; 0.039; 0.23; 0.15; 0.2];
dpc_f1 = [0.62; 0.66; 0.38; 0.31; 0.11; 0.19; 0.35; 0.34; 0.49; 0.26; 0.16; 0.071; 0.34; 0.42; 0.59; 0.78; 0.28];
dpc_ari = [0.56; 0.62; 0.37; 0.17; 0.093; 0.15; 0.34; 0.054; 9.4e-5; 0.23; 0.14; 0.054; 0.34; -0.0012; 0.31; 0.34; 0.1];

table_cdp_real = table(cdp_acc, cdp_nmi, cdp_f1, cdp_ari, dpc_acc, dpc_nmi, dpc_f1, dpc_ari);


%% Alg based on DCF, real dataset (11/RO/..)

alg_dcf_f1 = [0.69; 0.67; 0.7; 0.91; 0.8; 0.65; 0.63; 0.8; 0.55; 0.46; 0.39];
alg_dcf_ri = [0.55; 0.86; 0.55; 0.94; 0.87; 0.51; 0.65; 0.96; 0.95; 0.96; 0.96];

dpc_f1 = [0.74; 0.58; 0.52; 0.84; 0.81; 0.51; 0.58; 0.61; 0.48; 0.45; 0.38];
dpc_ri = [0.67; 0.84; 0.5; 0.89; 0.87; 0.5; 0.72; 0.92; 0.93; 0.95; 0.96];

table_alg_dcf_real = table(alg_dcf_f1, alg_dcf_ri, dpc_f1, dpc_ri);


%% FKNN-DPC, synthetic datasets (6/S/..)

fknn_dpc_acc = [0.987; 1; 0.963; 1; 0.999; 1];
fknn_dpc_ami = [0.941; 1; 0.936; 1; 0.995; 1];
fknn_dpc_ari = [0.96; 1; 0.924; 1; 0.997; 1];

dpc_acc = [0.733; 1; 0.97; 1; 0.997; 1];
dpc_ami = [0.5; 1; 0.945; 1; 0.992; 1];
dpc_ari = [0.453; 1; 0.937; 1; 0.996; 1];

table_fknn_synth = table(fknn_dpc_acc, fknn_dpc_ami, fknn_dpc_ari, dpc_acc, dpc_ami, dpc_ari);


%% FKNN-DPC, real datasets (13/RO/..)

fknn_dpc_acc = [0.973; 0.752; 0.716; 0.768; 0.944; 0.703; 0.949; 0.924; 0.436; 0.851; 0.648; 0.648; 0.818];
fknn_dpc_ami = [0.912; 0.284; 0.655; 0.847; 0.679; 0.324; 0.831; 0.759; 0.508; 0.273; 0.001; 0.247; 0.832];
fknn_dpc_ari = [0.922; 0.355; 0.555; 0.718; 0.786; 0.35; 0.852; 0.79; 0.308; 0.391; 0.013; 0.253; 0.714];

dpc_acc = [0.887; 0.681; 0.684; 0.697; 0.613; 0.586; 0.882; 0.9; 0.361; 0.61; 0.65; 0.535; 0.665];
dpc_ami = [0.767; 0.238; 0.651; 0.588; 0.009; 0.318; 0.706; 0.717; 0.39; 0.201; 0.034; 0.184; 0.728];
dpc_ari = [0.72; 0.276; 0.55; 0.49; -0.011; 0.268; 0.672; 0.734; 0.214; 0.027; 0.078; 0.164; 0.56];

table_fknn_real = table(fknn_dpc_acc, fknn_dpc_ami, fknn_dpc_ari, dpc_acc, dpc_ami, dpc_ari);


end

