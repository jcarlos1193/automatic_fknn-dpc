function [rho, delta] = DPC_rho_delta(X, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Matrix distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dist_vector = pdist(X);
dist = squareform(dist_vector);
ND = size(X,1);
maxd = max(max(dist));
N = length(dist_vector);

if strcmp(varargin{1},'percentage')
    position = round( N * varargin{2} / 100);
    sda = sort(dist_vector);
    dc = sda(position);
else
    dc = varargin{2};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcular rho, densidad de cada punto
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% dc = opt_dc;

rho = zeros(1, ND);

% Gaussian kernel

for i = 1 : ND - 1
    for j = i + 1 : ND
        rho(i) = rho(i) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
        rho(j) = rho(j) + exp( -(dist(i,j) / dc) * (dist(i,j) / dc) );
    end
end

[~, ordrho] = sort(rho, 'descend');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcular delta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

delta(ordrho(1)) = -1.;
nneigh(ordrho(1)) = 0;

for ii = 2 : ND

    delta(ordrho(ii)) = maxd;
    
    for jj = 1 : ii - 1
        
        if(dist(ordrho(ii),ordrho(jj)) < delta(ordrho(ii)))
            delta(ordrho(ii)) = dist(ordrho(ii),ordrho(jj));
            nneigh(ordrho(ii)) = ordrho(jj);
        end
     
    end
    
end

delta(ordrho(1)) = max(delta(:));

end

