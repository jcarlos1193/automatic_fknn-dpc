clear;
clc;
close;

addpath(genpath('..\Auxiliary_functions'));
addpath(genpath('..\DPC_Functions'));
addpath(genpath('..\Results'));


%%%%%%%%%% ------------- Experimentos 1 y 2 --------------- %%%%%%%%%%

% V-Measure

load('..\Results\Results_v1\RESULTS_vmeasure.mat')
t_vmeasure_e1 = table_vmeasure(:,1:8);

load('..\Results\Results_v1\RESULTS_DATASETS_DPC.mat')
t_vmeasure_e1.DPC = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC.mat')
t_vmeasure_e1.FKNN = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_DPC_Impr.mat')
t_vmeasure_e1.DPC_c_dc = T.V_Measure .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC-Impr.mat')
t_vmeasure_e1.FKNN_k = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_vmeasure_v4_1\RESULTS_DATASETS_FKNN-DPC-Impr-v4_1.mat')
t_vmeasure_e1.FKNN_c_k = T.V_Measure .* 100;

load('..\Results\Results_PCA_K_v3\RESULTS_DATASETS_FKNN-DPC-Impr-v3.mat')
t_vmeasure_e1.FKNN_k_ig = T.V_Measure .* 100;

load('..\Results\Results_centroides_pca_k_ig_v4\RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat')
t_vmeasure_e1.FKNN_c_k_ig = T.V_Measure .* 100;

% Purity

load('..\Results\Results_v1\RESULTS_purity.mat')
t_purity_e1 = table_purity(:,1:8);

load('..\Results\Results_v1\RESULTS_DATASETS_DPC.mat')
t_purity_e1.DPC = T.Purity .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC.mat')
t_purity_e1.FKNN = T.Purity .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_DPC_Impr.mat')
t_purity_e1.DPC_c_dc = T.Purity .* 100;

load('..\Results\Results_v1\RESULTS_DATASETS_FKNN-DPC-Impr.mat')
t_purity_e1.FKNN_k = T.Purity .* 100;

load('..\Results\Results_centroides_pca_k_vmeasure_v4_1\RESULTS_DATASETS_FKNN-DPC-Impr-v4_1.mat')
t_purity_e1.FKNN_c_k = T.Purity .* 100;

load('..\Results\Results_PCA_K_v3\RESULTS_DATASETS_FKNN-DPC-Impr-v3.mat')
t_purity_e1.FKNN_k_ig = T.Purity .* 100;

load('..\Results\Results_centroides_pca_k_ig_v4\RESULTS_DATASETS_FKNN-DPC-Impr-v4.mat')
t_purity_e1.FKNN_c_k_ig = T.Purity .* 100;

% Tablas Latex Experimento 1

latextable(table2cell(t_purity_e1(:,1:size(t_purity_e1,2)-2)), 'Horiz', t_purity_e1.Properties.VariableNames(1:size(t_purity_e1,2)-2), 'VLine', [0, 1, 8, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E1_LATEX_purity.tex');
latextable(table2cell(t_vmeasure_e1(:,1:size(t_vmeasure_e1,2)-2)), 'Horiz', t_vmeasure_e1.Properties.VariableNames(1:size(t_vmeasure_e1,2)-2), 'VLine', [0, 1, 8, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E1_LATEX_vmeasure.tex');

% Tablas Latex Experimento 2

latextable(table2cell(t_purity_e1(:,[1,12:size(t_purity_e1,2)])), 'Horiz', t_purity_e1.Properties.VariableNames([1,12:size(t_purity_e1,2)]), 'VLine', [0, 1, 8, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E2_LATEX_purity.tex');
latextable(table2cell(t_vmeasure_e1(:,[1,12:size(t_vmeasure_e1,2)])), 'Horiz', t_vmeasure_e1.Properties.VariableNames([1,12:size(t_vmeasure_e1,2)]), 'VLine', [0, 1, 8, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E2_LATEX_vmeasure.tex');


%%%%%%%%%% ------------- Rankings --------------- %%%%%%%%%%

t_vmeasure_e1{9,4} = 0.0;
t_vmeasure_e1{13,4} = 0.0;
t_vmeasure_e1{16,4} = 0.0;

[T_paired_rank_e1, ~, p_value_e1] = get_ranking_algorithms(t_vmeasure_e1{:,2:size(t_vmeasure_e1,2)-2}, t_vmeasure_e1.Properties.VariableNames(2:size(t_vmeasure_e1,2)-2));
[T_paired_rank_e2, mean_ranks_e2, p_value_e2] = get_ranking_algorithms(t_vmeasure_e1{:,2:size(t_vmeasure_e1,2)}, t_vmeasure_e1.Properties.VariableNames(2:size(t_vmeasure_e1,2)));


%%%%%%%%%% ------ Test de significatividad ------- %%%%%%%%%%

% Ranking Experimentos 1 y 2

C_paired_rank_e1 = table2cell(T_paired_rank_e1);
C_paired_rank_e2 = table2cell(T_paired_rank_e2);

alg_significant = zeros(size(t_vmeasure_e1, 2) - 1);

for idx_col = 1 : size(t_vmeasure_e1, 2) - 1
    
    for idx_row = idx_col : size(t_vmeasure_e1, 2) - 1
        
        [p_value, ~, ~] = friedman(t_vmeasure_e1{:, [idx_row + 1, idx_col + 1]}, 1, 'off');
        
        if p_value < 0.05
            
            alg_significant(idx_row, idx_col) = 1;
            C_paired_rank_e2{idx_row, idx_col} = '*';
            
            if idx_row <= size(C_paired_rank_e1, 1) && idx_col <= size(C_paired_rank_e1, 2)
                C_paired_rank_e1{idx_row, idx_col} = '*';
            end
            
        else
            
            C_paired_rank_e2{idx_row, idx_col} = ' ';
            
            if idx_row <= size(C_paired_rank_e1, 1) && idx_col <= size(C_paired_rank_e1, 2)
                C_paired_rank_e1{idx_row, idx_col} = ' ';
            end

        end
 
    end
    
end

alg_names = t_vmeasure_e1.Properties.VariableNames(2:size(t_vmeasure_e1,2));
t_alg_significant = array2table(alg_significant, 'VariableNames', alg_names, 'RowNames', alg_names);

% Tablas Latex

latextable(C_paired_rank_e1, 'Vert', T_paired_rank_e1.Properties.VariableNames, 'Horiz', T_paired_rank_e1.Properties.VariableNames, 'VLine', [0, 1, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E1_LATEX_ranking.tex');
latextable(C_paired_rank_e2, 'Vert', T_paired_rank_e2.Properties.VariableNames, 'Horiz', T_paired_rank_e2.Properties.VariableNames, 'VLine', [0, 1, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E2_LATEX_ranking.tex');


%%%%%%%%%% ------ Ranking de algoritmos segun los rangos medios ------- %%%%%%%%%%

[ord_ranks, ord_idx] = sort(mean_ranks_e2, 'descend');

list_sorted_algs = alg_names(ord_idx);
list_p_value = zeros(length(ord_idx), 1);

for idx = 1 : length(ord_idx) - 1
    
    idx_alg1 = ord_idx(idx);
    idx_alg2 = ord_idx(idx + 1);
    
    [p_value, ~, ~] = friedman(t_vmeasure_e1{:, [idx_alg1 + 1, idx_alg2 + 1]}, 1, 'off');
    
    list_p_value(idx) = p_value;
    
end

T_ord_algorithms = table(list_sorted_algs', ord_ranks', list_p_value, 'VariableNames', {'Algorithm', 'Mean_Rank', 'p_value'});

latextable(table2cell(T_ord_algorithms), 'Horiz', T_ord_algorithms.Properties.VariableNames, 'VLine', [0, NaN], 'HLine', [0, 1, NaN], 'format', '%.2f', 'name', '..\Results\Results_LATEX\RESULTS_E2_LATEX_ord_algs.tex');